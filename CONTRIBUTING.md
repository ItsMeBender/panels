# Contributing

Hi, it's nice if you want to contribute in this project.

Currently the impact of the project is small. I have one goal;

> Creating a tool for flexible panels.

If you have other ideas, improvements, please add them.

__ItsMeBender__  
_Bending the future._
