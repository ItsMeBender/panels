/*global describe it */

// Mocha allows you to use any assertion library you wish.
// In the example, we're using Node.js' built-in assert module — but generally, 
// if it throws an Error, it will work! This means you can use libraries such as: 
//     expect.js - expect() style assertions
//     chai - expect(), assert() and should-style assertions

var assert = require('assert');

describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.strictEqual([1, 2, 3].indexOf(4), -1);
    });
  });
});
