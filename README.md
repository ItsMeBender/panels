When working on structuring (layouting) a web page with a lot of dynamic information. 
There are some tools to manage these container blocks.
Depending on it's usage you can use `<TABLE>`, `<DIV>`, etc. 
Under control of CSS styling. Things like `float`, `flex-box`, `grid`.

What I need and try to solve with this package is giving a more dynamic layout to these containers.

The use case is that a user has a wide range of different data on his screen. 
Placed in containers with different dimensions. And these contaners can grow or shrink in size, 
to allow the user to zoom in / out on data. 
All this information on screen represents some sort of dashboard / cockpit of a wide range of data.


## Install

* Download this repo
* `npm install`
* `nmp start` will generate the mock-up site in `./build/` folder.
* `nmp run server` will generate a server to show the example in the `./build/` folder.



## Concept

The browser display is divided into a grid of columns and rows.
Whereby the column side is the most important one.
Because content demands a certain column width.
Rows can be infinite, because a browser scrolls down.
There is no physical border.

But if you create a big grid (small columns and rows), 
you have more control in claiming unused whitespace on screen.

What this package does is to group (connecting) grid cells, 
as space (rows &times; columns) to be used by a panel (content container).
Currently this can be achived with the CSS `GRID` property.
But this approach is difficult to make it more dynamically.

> _"What if you need more columns and or rows?"_

This package is dynamically mapping grid to panel space needs.



## How to work with

{{TODO}}
