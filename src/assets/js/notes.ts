import {  } from "../../../typings/notes";
import { Preset } from "./presets.js";  // Because it is a JS Class
import { Panel } from "../../../typings/panels.js";
import { StateSet } from "../../../typings/presets";

let lastNoteID = 0

class Note {
    id:string
    noteAuthor:string
    noteCreated:Date
    noteText:string
    presetId:string
    presetName:string
    panelIds:string[]

    constructor (noteAuthor:string, noteCreated:Date, noteText:string, presetID:string, presetName:string, panelIds:string[]) {
        this.noteAuthor = noteAuthor
        this.noteText = noteText
        this.noteCreated = noteCreated

        this.presetId = presetID
        this.presetName = presetName

        this.panelIds = panelIds

        this.id = lastNoteID.toString()

        lastNoteID += 1
    }
}

// A CLASS to handle the list of USER and SYTEM PRESETS.
class Notes {
    notes:Note[]

    constructor () {
        this.notes = []
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE methods
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * DEMO ONLY
     * Returns a random DATE, between two dates.
     */
    private __randomDate = (start:Date, end:Date):Date => {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }

    /**
     * Filter function on `note.noteCreated`.
     */
    private compareNoteCreated = (a:any, b:any):any => {
        return a.noteCreated < b.noteCreated
    }

    /**
     * Return a Note object, identified by `id`.
     * @param id of the Note
     */
    private getNoteById (id:string):Note {
        return (<any>this.notes).find((n:Note) => n.id === id )
    }

    /**
     * Returns all notes of a given `preset.id`.
     * @param id of the PRESET to filter notes
     */
    private getNotesByPresetId (id:string):Note[] {
        return (<any>this.notes).filter((n:Note) => {
            if (n.presetId === id) {
                return n
            }
        })
    }

    /**
     * Order notes list on date
     * @param n notes list to order
     */
    private sortNotes (n:Note[]):Note[] {
        // TODO: introduce SORT function
        n.sort(this.compareNoteCreated)
        return n
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // PUBLIC methods
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * DEMO ONLY - BERICHTEN VAN en NAAR
     * A the NOTES database with random notes.
     * Notes are connected to PRESETS (which will be ALL)
     */
    public __demoFillDatabaseNotes_disabled = (presets:Preset[]):void => {
        let texts:string[] = [
            'Mee eens',
            'Verandering in medicatie',
            'Voorstel voor veranderen van medicatie plan',
            'Patiënt heeft een historie betreffende ...',
            'Wil een tweede onderzoek ...',
            'Cliënt maakt kenbaar dat ...',
            'Besproken met patiënt ...',
            'Veel tekst: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
         ]
        let users:string[] = ['me', 'Bakker, B. (POH)', 'Smit, S. (Arts)', 'Dijk van, D. (Podotherapeut)', 'Lasser, L. (Diëtist)' ]

        presets.forEach((preset:Preset) => {
            // A preset can have multiple notes
            let idx:number
            let numberOfNotes = Math.floor(Math.random() * 5) // 0 .. 4
            let panelIds:string[] = []
            // Skip the following PRESETS, not to be filled with NOTES.
            // DEMO SHORTCUT, we need preset ID's.
            let skipPresets = ['Toevoegen paneel (system)', 'Notities (system)']
            let skipPanels = ['appl-notes', 'system-addPanel']

            if (skipPresets.indexOf(preset.label) < 0) {

                preset.panelSet.forEach((set:StateSet) => {
                    // Do not add NOTES relations to these PANELS
                    if (skipPanels.indexOf(preset.id) < 0) {
                        panelIds.push(set.id!)
                    }
                })
                
                for (idx = 0; idx < numberOfNotes; idx += 1) {
                    this.notes.push(new Note (
                        users[Math.floor(Math.random() * users.length)],
                        this.__randomDate(new Date(2018, 0, 1), new Date()),
                        texts[Math.floor(Math.random() * texts.length)],
                        preset.id,
                        preset.label,
                        panelIds
                    ))
                }
            }
        })

        // Order notes on Date
        this.notes = this.sortNotes(this.notes)
    }
   
    /**
     * DEMO ONLY - ONLY NOTES
     * A the NOTES database with random notes.
     * Notes are connected to PRESETS (which will be ALL)
     */
    public __demoFillDatabaseNotes = (presets:Preset[]):void => {
        let texts:string[] = [
            'Mee eens',
            'Verandering in medicatie',
            'Voorstel voor veranderen van medicatie plan',
            'Patiënt heeft een historie betreffende ...',
            'Wil een tweede onderzoek ...',
            'Cliënt maakt kenbaar dat ...',
            'Besproken met patiënt ...',
            'Veel tekst: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
         ]
        let users:string[] = ['me']

        presets.forEach((preset:Preset) => {
            // A preset can have multiple notes
            let idx:number
            let numberOfNotes = 1 // Math.floor(Math.random() * 5) // 0 .. 4
            let panelIds:string[] = []
            // Skip the following PRESETS, not to be filled with NOTES.
            // DEMO SHORTCUT, we need preset ID's.
            let skipPresets = ['Toevoegen paneel (system)', 'Notities (system)']
            let skipPanels = ['appl-notes', 'system-addPanel']

            if (skipPresets.indexOf(preset.label) < 0) {

                preset.panelSet.forEach((set:StateSet) => {
                    // Do not add NOTES relations to these PANELS
                    if (skipPanels.indexOf(preset.id) < 0) {
                        panelIds.push(set.id!)
                    }
                })
                
                for (idx = 0; idx < numberOfNotes; idx += 1) {
                    this.notes.push(new Note (
                        users[Math.floor(Math.random() * users.length)],
                        this.__randomDate(new Date(2018, 0, 1), new Date()),
                        texts[Math.floor(Math.random() * texts.length)],
                        preset.id,
                        preset.label,
                        panelIds
                    ))
                }
            }
        })

        // Order notes on Date
        this.notes = this.sortNotes(this.notes)
    }
   
    /**
     * Add a NOTE to the database
     */
    public add = ({noteAuthor, noteCreated, noteText, preset}: {noteAuthor:string; noteCreated:Date; noteText:string; preset:Preset}) => {
        let panelIds:string[] = []

        // Build an array of PANEL ID's, related to this new NOTE.
        preset.panelSet.forEach((set:StateSet) => {
            panelIds.push(set.id!)
        })

        this.notes.push(new Note (noteAuthor, noteCreated, noteText, preset.id, preset.label, panelIds))

        this.updateNotesView([preset])
    }

    /**
     * Returns a formatted string, containing a number of days after an EVENT (date)
     * Format: "T+0" (Today), "T-##" (Number of days before event), "T+##" (Number of days after event)
     * @param d an EVENT date
     */
    public dateToDays (d:Date):string {
        let today = new Date()
        let daysApart = Math.abs(Math.round((<any>today - <any>d)/86400000));

        return daysApart >= 0 ? 'T+' + daysApart.toString() : 'T-' + daysApart.toString()
    }

    /** 
     * Initialize Application
     * - Add mouse click event listener, to monitor NOTE clicks.
     */
    public init () {
        let notesWrapper:HTMLElement = document.getElementById('notesViewNotesList')!
            
        notesWrapper.addEventListener('mouseover', (e:any) => {
            let target = e.target!

            // Get row PRESET.ID when clicked.
            while (target && (target as HTMLElement).nodeName.toUpperCase() !== 'LI') {
                target = target.parentElement
            }

            if (target) {
                this.getNoteById(target.dataset.noteId)!
                .panelIds
                .forEach((id:string) => {
                    document.getElementById(id)!.classList.add('note--panelRelation')
                })
            }
        })

        
        notesWrapper.addEventListener('mouseout', (e:any) => {
            let target = e.target!

            // Get row PRESET.ID when clicked.
            while (target && (target as HTMLElement).nodeName.toUpperCase() !== 'LI') {
                target = target.parentElement
            }

            if (target) {
                this.getNoteById(target.dataset.noteId)!
                .panelIds
                .forEach((id:string) => {
                    document.getElementById(id)!.classList.remove('note--panelRelation')
                })
            }

        })
    }

    /**
     * Update the PANEL NOTES, with all the notes from given PRESETS.
     * Meaning, panels currently visible on screen.
     * Some presets, do contain other SYSTEM presets like ['appl-notes', 'system-addPanel'] (these are single panels).
     * They are NOT part of the note relations. 
     * A preset do contain panels like 'appl-notes', but the dom not activate notes, related to this preset.
     */
    public updateNotesView = (presets:Preset[]):void => {
        let hasSummary = false
        let headerNotes = document.getElementById('notesViewHeader')!
        let options = {hour: "2-digit", minute: "2-digit"};
        let showNotesLst:Note[] = []
        let summaryContainer = document.getElementById('notesViewSummary')!
        let summary:string
        let ulContainer = document.getElementById('notesViewNotesList')!

        // Retrieve NOTES from these presets
        presets.forEach((preset:Preset) => {
            showNotesLst = showNotesLst.concat(this.getNotesByPresetId(preset.id))
        })
        
        showNotesLst = this.sortNotes(showNotesLst)

        // Clear current view
        while (ulContainer.firstChild) {
            ulContainer.removeChild(ulContainer.firstChild);
        }

        // Reset
        headerNotes.innerHTML = '-'
        summaryContainer.innerHTML = '-'

        // Build new list
        showNotesLst.forEach((note:Note) => {
            let elLI = document.createElement('LI')

            // Needed to make a connection
            elLI.dataset.noteId = note.id

            // "note--recieved", "note--sent"
            if (note.noteAuthor === 'me') {
                elLI.setAttribute('class', 'note--sent')
                summary = `
                    <span class="author">Notitie bij preset '${note.presetName}'</span><br>
                    <p>${note.noteText}</p>
                    <span class="dateCreated"><u>[wijzig]</u> ${this.dateToDays(note.noteCreated)} (${note.noteCreated.toLocaleTimeString('nl-NL', options)})</span>
                `
                elLI.innerHTML = summary
            } else {
                elLI.setAttribute('class', 'note--recieved')
                summary = `
                    <span class="author">${note.noteAuthor}</span> -
                    <span class="author">Notitie bij preset '${note.presetName}'</span><br>
                    <p>${note.noteText}</p>
                    <span class="dateCreated">${this.dateToDays(note.noteCreated)} (${note.noteCreated.toLocaleTimeString('en-US', options)})</span>
                `
                elLI.innerHTML = summary
            }

            ulContainer.appendChild(elLI)

            // Show the LATEST NOTE in the summary (newest NOTE date)
            if (!hasSummary) {
                hasSummary = true
                headerNotes.innerHTML = `
                    <span class="data">${this.dateToDays(note.noteCreated)}</span> <span class="data">Van: ${note.noteAuthor}</span> <span class="title">Notities</span><br>
                    <span class="history"># Notities</span>
                `
                summaryContainer.innerHTML = summary
            }
        })

        // IN CASE THERE ARE NO NOTES (Summary panel has not been set)
        // Add a message to add panels to the notes view. Because panels (presets) act as filters for the notes
        if (!hasSummary) {
            hasSummary = true
            headerNotes.innerHTML = `
                <span class="data">Voeg panelen toe voor notitie overzicht</span><br>
                <span class="history"># notities</span>
            `
            summary = `
                <span class="author">Xxxx</span> -
                <span class="author">Notitie bij preset 'Xxxx'</span><br>
                <p>Xxxx</p>
                <span class="dateCreated">Xxxx</span>
            `
            summaryContainer.innerHTML = summary
        }
    }
}

export {Notes}
