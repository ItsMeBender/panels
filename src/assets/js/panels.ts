import { GridCell, GridSize, PanelFilter, PanelStateSet, Semaphore, SemaphoreType, State, Sticky } from "../../../typings/panels";
import { StateSet } from "../../../typings/presets";


/**
 * A PANEL is a container for data on screen.
 * The panel has a location and size. Both depending on a virtual GRID of columns and rows.
 * The GRID is dependent of the browser viewport. Low resolution, less grid colums (content driven).
 * Grid ROWS, is not that important. Because users are used to scrolling up and down (vertically).
 * The panel sizes, depends of the CONTENT of the panel. In this demo a panel can have different kinds of content.
 * So the Panel size is also dynamic.
 * The goal for PANELS is to fill the users viewport in optimal space usage. Filling panel gaps, with smaller panels.
 * Panel can dynamically switch visual location. Because of the space panels are using.
 * But internally, the HTML DOM, stays the same. Only the physical location on screen is changing,
 * depending on ordering RULES:
 * - preferred panel ordering (alphabetical, groups, TTdates, user preferences)
 * - Space in the GRID (small panels will use the gaps, left by larger panels.)
 * 
 * In this demo a PANEL will have a HEADER, CONTENT and a FOOTER.
 * A panel has states, which represents the panel content usage.
 * Currently we have 4 states:
 * - MINIMAL, only the HEADER is visible, identifying the DATA, INFORMATION
 * - SUMMARY, HEADER and a CONTENT block, with a SHORT, CRYPTIC description of the DATA.
 * - EXPANDED, More detailed DATA presentation, like a TABEL or a LIST.
 * - ENLARGED, a next level of data presentation, like a Graph.
 */

// tsc *.ts --watch

class Panel {
    // Is PANEL active (visible in the viewport).
    active:boolean
    // Available STATES for this panel; 'MINIMAL', 'SUMMARY', ...
    availableStates:State[]
    columnSize:number
    currentState:State
    defaultActive:boolean
    defaultSeq:number
    defaultState:State
    el: HTMLElement
    // If MEDIA content is available, to avoid clicking on empty data
    hasData: boolean
    id:string
    previousState:State
    rowSize:number
    semaphore:Semaphore
    semaphoreType:SemaphoreType
    seq:number
    startColumn:number
    startRow:number
    // Fixed PANEL order behaviour
    sticky:Sticky
    title:string // Used to sort on, find option

    constructor({ id, state, seq }: { id:string; state:State; seq:number; }) {
        let title:HTMLCollection

        this.active = true
        this.defaultActive = true
        this.el = document.getElementById(id)!
        this.columnSize = 0 // undefined
        this.currentState = state 
        this.defaultState = state // State as open by user first moment
        this.defaultSeq = seq // Start at `0` and sequential up
        this.hasData = false // Contains no MEDIA (data)
        this.id = id
        this.rowSize = 0 // undefined
        this.seq = seq // Current display sequence
        this.startColumn = 0 // Needs to be calculated on grid specifications
        this.startRow = 0 // Needs to be calculated on grid specifications
        this.sticky = 'NONE' // Panel is relocatable by user

        title = this.el.getElementsByClassName('title')!
        this.title = title[0].innerHTML

        // Available panel states
        this.availableStates = []

        // Set default behaviour when only minimal state is active
        if (state === 'MINIMAL') {
            this.previousState = 'SUMMARY'
        } else {
            this.previousState = 'MINIMAL'
        }

        // Check if MEDIA content available
        if ((<HTMLElement>this.el.getElementsByClassName('panel__content')[0]).firstElementChild!) {
            this.hasData = true
            this.semaphore = 'MEDIUM' // TODO: Make dynamic
            this.semaphoreType = 'EQUAL'
        } else {
            // No data presented
            this.semaphore = 'NONE'
            this.semaphoreType = 'NONE'
        }
    }

    /**
     * Sets the panel sequence number for the HTML element attribute `data-panel-seq`.
     * And the internal property `panel.seq`.
     * The attribute starts with '1', the property with '0'. 
     * @param seqNo sequence number of this panel
     */
    public setSeqNo (seqNo:number):void {
        this.el.dataset.panelSeq = (seqNo + 1).toString()
        this.seq = seqNo
    }
}

class Panels {
    // Properties
    dimension: GridSize
    grid: GridCell[]
    gridCount:number
    panels: Panel[] // Complete list of inactive and active panels.
    panelViews: Panel[] // List of active, visible panels.

    constructor(dimension: GridSize) {
        this.dimension = {columns: 0, rows: 0}
        this.grid = []
        this.gridCount = 0
        this.panels = []
        this.panelViews = []

        // Set GRID dimension in columns and rows
        this.setGridDimension(dimension)
        // Scan for HTML containers, identified as PANELS, using `<DIV class="panel">`.
        this.scanHTMLForPanels()
        // Map panel definition size (width as columns, height as rows), to internal GRID
        this.mapPanelsToGrid()
        // Calculate physical panel dimension, as seen on screen
        this.sizePanelsToWindow()
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE methods
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Is used for DEBUGGING, just to check if the `panel.seq` in the given list `panelLst`.
     * start with 0 and is sequential (0, 1, 2, 3 , ...)
     * @param panelLst of panels to validate the `panel.seq` sequence.
     */
    private _checkSeqChecksum(panelLst:string[]):boolean {
        let checkSum = 0
        let checkSumCtrl = 0
        let str = ''

        panelLst.forEach((id:string, idx:number) => {
            let panel = this.findPanelByID(id)
            checkSum += panel.seq
            checkSumCtrl += idx
            str += str ? ` | ${idx}:${panel.seq}` : `${idx}:${panel.seq}`
        })

        // Check checksums for the `panelLst.seq` ordering and validity.
        // If incorrect will lead to unexpected errors.
        if (checkSum !== checkSumCtrl) {
            console.error(`Preset has incorrect 'StateSet.seq' numbering`)
            console.error(str)
            return false
        }
        return true
    }

    /**
     * Add rows the GRID, to add more room for PANEL `state` expansion.
     * @param count the number of new to be added rows to the grid.
     */
    private addRowsToGrid (count:number):void {
        let idx = this.grid.length // First new gridCell
        let addCellsMax = count * this.dimension.columns + idx

        this.dimension.rows += count
        this.gridCount = this.dimension.rows * this.dimension.columns

        for ( ; idx < addCellsMax; idx += 1 ) {
            // Grid COLUMNS and ROWS, starting from 0,0
            let column = 0
            let row = 0
    
            row = Math.floor(idx / this.dimension.columns)
            column = idx - (row * this.dimension.columns)

            this.grid.push({column:column, row:row, seq:idx, panelID:""})
        }
    }

    /**
     * Depending on the web browser screen width and the number of columns.
     * All columns will have the same physical width in pixels.
     * Only the last column will also have the remainder of the devision (size/columns)
     * Forcing total column width to be equal to screen width.
     * @returns an array of column widths in pixels. 
     */
    private calculateColumnWidth ():number[] {
        const body = document.getElementsByTagName('BODY')[0]!
        const columns:number[] = []
        const innerWidth:number = body.clientWidth

        let width:number = Math.floor(innerWidth / this.dimension.columns)
        let idx

        for (idx = 0; idx < this.dimension.columns; idx += 1) {
            if (idx === (this.dimension.columns - 1)) {
                // Last column takes also the remainder
                // Window width minus already used column widths
                width = innerWidth - (width * (this.dimension.columns - 1))
            }
            columns.push(width) // pixels
        }

        return columns
    }

    /**
     * Returns the GRID CELL sequence number, based on a starting point `gridSeq`.
     * And the relative `column` and `row` number from that starting point.
     * @param gridSeq is the start GRID CELL to count `column` and `row` from
     * @param column is the relative number of columns to add to `gridSeq` starting point.
     * @param row is the relative number of rows to add to `gridSeq` starting point.
     * @returns the relative GRID CELL seq number, on cooridinates `column` and `row`.
     */
    private getRelativeGridID (gridSeq:number, column:number, row:number):number {
        const gridCell:GridCell = this.grid[gridSeq]

        let aCol = gridCell.column + column
        let aRow = gridCell.row + row

        if (aCol < 0 || aRow < 0 || aCol >= this.dimension.columns || aRow >= this.dimension.rows) {
            return -1 // No GRIDCELL for this position
        }
        return gridCell.seq + column + row * this.dimension.columns
    }

    /**
     * Return true when view port contain a scrollBar.
     * Meaning decrease panels in size.
     */
    private hasScrollBar ():boolean {
        const body = document.getElementsByTagName('BODY')[0]!
        // Using `body.clientWidth` instead of `window.clienWidth`, because `body.scrollWidth`
        // is adjusted after second PANEL render to compensate.
        return body.clientWidth < body.scrollWidth
    }

    /**
     * A PANEL has a given state DIMENSION (columns, rows), which claims space in the GRID.
     * This function walks to all PANELS and find a free spot in the GRID.
     * For each panel, the free spot grid search starts at TOP, LEFT.
     * Resulting in small panels at the bottom, placed between other panels if there is room.
     * Also giving it a bit of random behaviour, where panels can be found.
     * @param sorting 
     */
    private mapPanelsToGrid (sorting?:Function) {
        let relation:number = -1
        let sort:string[] = []
        let pingLst:string[] = []

        this.grid = this.resetGrid()

        // Check if a SORT() function is provided
        if (sorting) {
            // Use given `sorting` function.
            sort = sorting()
        } else {
            // Use default sort sequence as given by panelViews[].seq
            this.panelViews.forEach ((panel:Panel) => {
                // panel.seq starts at `0`, but attribute data-panel-seq="1", starts at one!
                sort[panel.seq] = panel.id
            })
        }

        // Allocate Panel SIZE to GRID CELLS.
        // If initial GRID is FULL, then add more GRID ROWS.
        sort.forEach( (id:string, idx:number) => {
            const panel = this.findPanelByID(id)

            let col:number
            let prevStartColumn = panel.startColumn
            let prevStartRow = panel.startRow


            // Check for columns widths
            // TODO: Ik denk verkeerde plek, beter by state settinge
            if (panel.columnSize > this.dimension.columns) {
                panel.columnSize = this.dimension.columns
            }

            panel.setSeqNo(idx)

            // Find TOP, LEFT gridCell which can contain this PANEL SIZE.
            relation = this.searchFreeGridSize(panel.columnSize, panel.rowSize)

            while (relation === -1) {
                // No suitable free GRID spavce found
                // Add more rows to grid
                this.addRowsToGrid(1)
                relation = this.searchFreeGridSize(panel.columnSize, panel.rowSize)
            }

            panel.startColumn = this.grid[relation].column
            panel.startRow = this.grid[relation].row

            // Check if new panel POSITION, then ping panel.
            if (prevStartColumn !== panel.startColumn || prevStartRow !== panel.startRow) {
                pingLst.push(panel.id)
            }

            // GridCell 0 TOP LEFT, always at least one gridCell (1x1)
            // Claim gridCell for this panel
            for (col = 0; col < panel.columnSize; col += 1) {
                let row:number
                for (row = 0; row < panel.rowSize; row += 1) {
                    this.grid[this.getRelativeGridID(relation, col, row)].panelID = panel.id
                }
            }
            // this.dumpGrid()
        })

        // Ping moved panels.
        if ( pingLst.length > 0) {
            this.pingPanels(pingLst)
        }

    }

    /**
     * Reset grid by removing the claim of a PANEL ID.
     * Indicating, GRIDCELL is free to be used by other PANEL.
     * @returns {GridCell[]} cloned and updated GridCell array
     */
    private resetGrid ():GridCell[] {
        return this.grid.map( cell => { 
                return {
                    row: cell.row, 
                    column: cell.column, 
                    seq: cell.seq, 
                    panelID: ''
                }
            }
        )
    }

    /**
     * Scan the HTML document <BODY> for PANEL definitions, identified by `class="panel"`
     * This HTML-element must contain the following attributes;
     * - data-panel-default-state="minimal|summary|expanded|enlarged" - The panel content render size at first paint.
     * - data-panel-seq="#" - The render sequence number of the PANEL, start with 1 and is sequential!
     * - data-grid-col="#" - The PANEL `column` size when PANEL `state` is `MINIMAL`
     * - data-grid-row="#" - The PANEL `row` size when PANEL `state` is `MINIMAL`
     * 
     * The PANEL container has also a container for MEDIA content. Indentified by `class="panel__content"`.
     * This container supports containers with user data for all the states supported.
     */
    private scanHTMLForPanels ():void {
        // Get a list of all PANEL containers
        const htmlPanels:HTMLCollection = document.getElementsByClassName('panel')

        let maxEl = htmlPanels.length
        let idx:number
        let panelIdLst:string[] = [] // Used to support problem solving DEV's

        // Walk through PANEL list and administer HTML DIV.panel
        for (idx = 0; idx < maxEl; idx +=1 ) { // TODO: CleanCode
            const el:HTMLElement = <HTMLElement>htmlPanels[idx]!
            const panelStateSet:PanelStateSet = this.scanPanelStateDimensions(el.id)

            let panel = new Panel({
                id: el.id,
                state: <State>(<string>el.dataset.panelDefaultState).toUpperCase(),
                seq: parseInt(<string>el.dataset.panelSeq) - 1 // Seq number start at 0
            })
            let defaultState = <State>panel.defaultState.toUpperCase()
            let semaphore:HTMLCollection
            let quickPanelAccessDiv:HTMLCollection = el.getElementsByClassName('has_quick_panel_access')!
            let titleDiv:HTMLCollection = el.getElementsByClassName('header__title')!

            // VALIDATIONS ///////////////////////////////////////////////////////////////
            if (panelIdLst.indexOf(panel.id) < 0) {
                panelIdLst.push(panel.id)
            } else {
                // Duplicate found
                console.error(`A duplicate panel id="${panel.id}" found. Please check HTML.`)
            }

            // Store actual PANEL state dimensions
            panel.columnSize = panelStateSet[defaultState].columnSize
            panel.rowSize = panelStateSet[defaultState].rowSize

            // STORE PANEL SEMAPHORES ////////////////////////////////////////////////////
            // Scanning the HTML-DOM tree for clues on SVG icon used UP, DOWN and `attention` level.
            semaphore = el.getElementsByClassName('header__semaphore')!

            panel.semaphore = 'NONE'
            panel.semaphoreType = 'NONE'
            if (semaphore.length > 0) {
                // Semaphore ATTENTION level
                switch (true) {
                    case semaphore[0].classList.contains('attention_low'):
                        panel.semaphore = 'LOW'
                        break;
                    case semaphore[0].classList.contains('attention_medium'):
                        panel.semaphore = 'MEDIUM'
                        break;
                    case semaphore[0].classList.contains('attention_high'):
                        panel.semaphore = 'HIGH'
                        break;
                }
                // Semaphore DATA INDICATOR
                switch (true) {
                    case semaphore[0].innerHTML.search('arrow-downward') > 0:
                        panel.semaphoreType = 'DOWN'
                        break;
                    case semaphore[0].innerHTML.search('arrow-stop') > 0:
                        panel.semaphoreType = 'EQUAL'
                        break;
                    case semaphore[0].innerHTML.search('arrow-upward') > 0:
                        panel.semaphoreType = 'UP'
                        break;
                }    
            }

            // STORE PANEL STATES ////////////////////////////////////////////////////////
            // 'MINIMIZE' is always available
            panel.availableStates.push('MINIMAL')

            // Both dimensions not '0', meaning empty and not used, no correspondending HTML
            if (!(panelStateSet.SUMMARY.columnSize === 0 && panelStateSet.SUMMARY.rowSize === 0 )) {
                panel.availableStates.push('SUMMARY')
            }
            if (!(panelStateSet.EXPANDED.columnSize === 0 && panelStateSet.EXPANDED.rowSize === 0 )) {
                panel.availableStates.push('EXPANDED')
            }
            if (!(panelStateSet.ENLARGED.columnSize === 0 && panelStateSet.ENLARGED.rowSize === 0 )) {
                panel.availableStates.push('ENLARGED')
            }

            // EVENTS ////////////////////////////////////////////////////////////////////
            // Add CLICK event to TITLE, for quick peek or closing
            titleDiv[0].addEventListener('click', ((panel:Panel) => {
                return (e:Event) => {
                    this.setFocusToPanel(panel)
                    
                    if (panel.currentState.toLowerCase() === "minimal") {
                        // Avoid switching to the same state, user feedback must be the same
                        if (panel.previousState === 'MINIMAL') {
                            // Check if requested state is available
                            // In case of empty panels with only a TITLE bar (state 'MINIMAL')
                            if (panel.availableStates.indexOf((<State>'SUMMARY')) >= 0) {
                                this.setPanelState({id:panel.id, state:'SUMMARY'})
                            }
                        } else {
                            this.setPanelState({id:panel.id, state:panel.previousState})
                        }
                    } else {
                        // State `MINIMAL` always available
                        this.setPanelState({id:panel.id, state:'MINIMAL'})
                    }
                }
            })(panel))

            // If the `DIV.panel__content` don't have interactive STATES with FORMS, BUTTONS
            // Then clicking on them should open the NEXT panel state, until there are no more states.
            // State SEQUENCE 'MINIMAL'|'SUMMARY'|'EXPANDED'|'ENLARGED' ('CLOSE' not functional in this case)
            if (quickPanelAccessDiv.length > 0) {
                quickPanelAccessDiv[0].addEventListener('click', ((panel:Panel) => {
                    return (e:Event) => {
                        let currentSeq:number
                        let displaySequence:State[] = ['MINIMAL', 'SUMMARY', 'EXPANDED', 'ENLARGED'] // 0,1,2,3

                        currentSeq = displaySequence.indexOf(<State>panel.currentState.toUpperCase())

                        // Check if STATE exists and not last
                        if (!(currentSeq === -1 || currentSeq === 3)) {
                            // Switch to NEXT STAGE
                            currentSeq += 1

                            // Check if requested state is available for this panel
                            if (panel.availableStates.indexOf(displaySequence[currentSeq]) >= 0) {
                                this.setPanelState({id:panel.id, state:displaySequence[currentSeq]})
                            }
                        }
                    }
                })(panel))
            }


            // Panel ACTIVE state (visible, usable by user)
            panel.active = !panel.el.classList.contains('panel--inactive')
            panel.defaultActive = panel.active

            // Insert PANEL in array at the correct `data-panel-seq` index.
            this.panels[parseInt(<string>el.dataset.panelSeq) - 1] = panel
        }
        
        // Make panel view list
        maxEl = 0; // ';' NEEDED 
        (this.panels as []).forEach((panel:Panel) => {
            if (panel.active) {
                this.panelViews[panel.seq] = panel
                this.setPanelLayout (panel, panel.seq)
                maxEl += 1
            }
        })

        // Validate `panel` sequence numbers.
        if (maxEl !== this.panelViews.length) {
            console.error(`Panel 'data-panel-seq' error. Probably duplicates, sequence number must be unique. Check also class ".panel--inactive"` )
            return
        }
        // Starts with one (1) and is sequential
        this.panelViews.forEach((panel:Panel, idx:number) => {
            if (panel.defaultSeq !== idx) {
                console.error(`Panel 'data-panel-seq'sequence error. Always start with 'data-panel-seq="1"' and ascending.` )
                return
            }
        })
    }

    /**
     * Scan for all supported PANEL states ('MINIMAL'|'SYMMARY'|'EXPANDED'|'ENLARGED').
     * And returns an Object with specific panel state DIMENSIONS.
     * With this you can addapt the layout of the panel's specific state.
     * The state dimensions, are HTML attributes; `data-panel-state="state"`,
     * `data-grid-col="number"` `data-grid-row="number"`
     * @param data is the `id` of the PANEL or `Panel` object.
     * @returns PanelStateSet containing available state panel dimensions.
     */
    private scanPanelStateDimensions (data:string|Panel):PanelStateSet { 
        let contentEl:HTMLCollection
        let idx:number
        let maxEl:number
        let panelEl:HTMLElement
        let set:any = { // Based PanelStateSet
            MINIMAL: {columnSize: 0, rowSize: 0, el: null}, // (column, rows) 0,0 means also NOT-IN-USED
            SUMMARY: {columnSize: 0, rowSize: 0, el: null},
            EXPANDED: {columnSize: 0, rowSize: 0, el: null},
            ENLARGED: {columnSize: 0, rowSize: 0, el: null},
            CLOSE: {columnSize: 0, rowSize: 0, el: null}, // To use the preset close all button
        }

        if (typeof data === 'string' || data instanceof String) {
            panelEl = document.getElementById(<string>data)!
        } else {
            panelEl = data.el
        }
        set.MINIMAL.el = panelEl // Always panel `MINIMAL` available, because it contains panel TITLE
        contentEl = panelEl.getElementsByClassName('content__state')!
        maxEl = contentEl.length

        // Main PANEL container, defines the `minimal` panel SIZE
        set.MINIMAL.columnSize = parseInt(<string>panelEl.dataset.gridCol)
        set.MINIMAL.rowSize = parseInt(<string>panelEl.dataset.gridRow)

        for (idx = 0; idx < maxEl; idx += 1) {
            const state:string = (<HTMLElement>contentEl[idx]).dataset.panelState!.toUpperCase()

            let columnSize:number
            let rowSize:number
            let valC: any
            let valR: any

            // We do some HTML code checking, input validation TODO on more places
            if (state) {
                valC = parseInt((<HTMLElement>contentEl[idx]).dataset.gridCol!)
                columnSize = isNaN(valC) ? 0 : valC
                valR = parseInt((<HTMLElement>contentEl[idx]).dataset.gridRow!)
                rowSize = isNaN(valR) ? 0 : valR

                if (set.hasOwnProperty(state)) {
                    set[state].columnSize = columnSize
                    set[state].rowSize = rowSize
                    set[state].el = contentEl[idx]
                } else {
                    console.error(`Problem in panel div#'${panelEl.id}'. State '${state}' not one of ['minimal'|'symmary'|'enlarged'|'expanded']`)    
                }

                if (isNaN(valC) || isNaN(valR)) {
                    console.error(`Problem in panel div#'${panelEl.id}' State '${state}' 'data-grid-col|row' does not contain a number`)    
                }
            } else {
                console.error(`Problem in panel div#'${panelEl.id}' structure, no 'data-panel-state="..." found.'`)
            }
        }
        return set
    }

    /**
     * Search through the GRID MAP (columns * rows). to find a spot, panel free,
     * with a given dimension `colSize` x `rowSize`.
     * @param colSize The required free GRID space horizontally (columns)
     * @param rowSize The required free GRID space vertizontally (rows)
     * @return number of the first free GRID CELL, capable of hosting a panel of size (`colSize` x `rowSize`)
     * It can be also `-1`, indication no free space (You can then add more rows to the grid)
     */
    private searchFreeGridSize (colSize:number, rowSize:number):number {
        let freeGrid:number = -1 // Nothing free
        let gridCell:number

        for (gridCell = 0; gridCell < this.gridCount; gridCell += 1) {
            // find first free GRIDCELL with no `panelID`.
            if (this.grid[gridCell].panelID === "") {
                // Can we claim free grid space from this grid cell?
                let col:number
                let free:boolean = true
                for (col = 0; col < colSize; col += 1) {
                    let row:number
                    for (row = 0; row < rowSize; row += 1) {
                        let gridID = this.getRelativeGridID(gridCell, col, row) // Relatief from `gridCell`

                        if (gridID >= 0) {
                            if (this.grid[gridID].panelID !== "") {
                                free = false
                            }
                        } else {
                            // No gridCell outside GRID column, row range.
                            free = false
                        }
                    }
                }

                if (free) {
                    freeGrid = gridCell
                    break
                }
            }
        }
        return freeGrid
    }

    /**
     * TODO: If grid already is set, and changed, then all panels need to re-calculateColumnWidth 
     * But then panel states could use more columns then physical available. THIS IS PROBLEM.
     * 
     * Define the initial window COLUMN and ROW dimensions.
     * The column count is important! Because panels can not grow outsite the given GRID columns.
     * And we don't allow (yett) horizontal scrolling, because it's not used in real live.
     * ROWS are not a problem, because window vertical scroll.
     * @param dimension 
     */
    private setGridDimension (dimension: GridSize): void {
        let idx:number

        this.gridCount = dimension.columns * dimension.rows // Also length of array `this.grid`.

        this.dimension.columns = dimension.columns
        this.dimension.rows = dimension.rows

        for (idx = 0; idx < this.gridCount; idx += 1) {
            let column = 0
            let row = 0
    
            row = Math.floor(idx / this.dimension.columns)
            column = idx - (row * this.dimension.columns)
    
            // `column` is the column number in the GRID.
            // `row` is the row number in the GRID.
            // `seq` is just the GRIDCELL sequence number, identifying the GRID CELL.
            // `panelID` reverse to the PANEL claiming this GRID CELL.
            // "": PanelID unset, not in use by a panel
            this.grid.push({column:column, row:row, seq:idx, panelID:''})
        }
    }

    /**
     * All PANELS STATES do have a specified `panel.columnSize` and `panel.rowSize`.
     * This defines the SPACE a panel uses on the GRID (columns, rows).
     * The actual window screen `width` is divided by the column count. 
     * Resulting in the width per column. The last column has also the remainder to make it 100%.
     * This function will calculate the ABSOLUTE position of a panel (TOP, LEFT) and SIZE (WIDTH, HEIGHT) of a panel.
     * All depending on the panel location (panel.startColumn, panel.startRow) in the GRID.
     */
    private sizePanelsToWindow (): void {
        const _rowHeight = 64 // px, remember it needs room for panel margin
        // The last column, can have a different size. Includes a rest value.
        const columnWidths:number[] = this.calculateColumnWidth()

        let p:number
        let sort:string[] = []

        // Determine the current SORT ORDER, defined by `panel.seq`, starting from `0`.
        this.panelViews.forEach((panel:Panel) => {
            sort[panel.seq] = panel.id
        })

        for (p = 0; p < this.panelViews.length; p += 1) {
            const panel:Panel = this.findPanelByID(sort[p])

            let coordinate:string
            let gridColumn:number
            let panelHeightPx:number
            let panelWidthPx:number
            let panelTopPx:number
            let panelLeftPx = 0

            // Panel vertical dimensions
            panelHeightPx = panel.rowSize * _rowHeight
            panelTopPx = panel.startRow * _rowHeight

            // Panel horizontal dimensions
            for (gridColumn = 0, panelWidthPx = 0; gridColumn < this.dimension.columns; gridColumn += 1) {
                if (gridColumn >= panel.startColumn && gridColumn < panel.startColumn + panel.columnSize) {
                    panelWidthPx += columnWidths[gridColumn]
                }
            }
            for (gridColumn = 0, panelLeftPx = 0; gridColumn < panel.startColumn; gridColumn += 1) {
                panelLeftPx += columnWidths[gridColumn]
            }

            coordinate = `top:${panelTopPx}px; left:${panelLeftPx}px; height:${panelHeightPx}px; width:${panelWidthPx}px;`

            panel.el.setAttribute('style', coordinate)
        }

        // If new calculated and manipulated DOM invokes a vertical scrollBar
        // The FIXED COLUMN widths (in px) must be recalculated to compensate th visible vertical scrollBar.
        // TODO: This CPU (DOM-Tree) manipulation intensive. Better pre-calculate the total height of all
        // panels verticaly. And compome to the view port height and then caluculate the COLUMNS 
        // and THEN this 'sizePanelsToWindow' function.
        if (this.hasScrollBar()) {
            // Recalculate
            this.sizePanelsToWindow()
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // PUBLIC methods
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Activate these panels and make them visible to the user.
     * Allowing the user to interact with these new panels.
     * TODO: Panel selection on TOP
     * @param panels to activate, as in a list of panels.
     */
    public activatePanels (panelLst:string[]) {
        // let addPanelSeq = 0 // Default panel INSERT position
        let idx = 0
        let sort:any = []
        let stickyBottom:any = []
        let stickyTop:any = [] 

        // Add new PANELS to bottom of `panelView`
        panelLst.forEach((id:string) => {
            let panel = this.findPanelByID(id)
            // Check if the is NOT already active
            if (!panel.active) {
                // Make sure PANEL visible and ACTIVE in the VIEW
                panel.el.classList.remove('panel--inactive')
                panel.active = true
                panel.seq = this.panelViews.length // Place at the end of panel sequence
                // Push panel to the acttve list `panelViews`
                this.panelViews.push(panel)
            }
        })

        // Walk through now ACTIVE (visible) panels.
        // Order them in (none) STICKY groups
        for (idx = 0; idx < this.panelViews.length; idx += 1) {
            const activePanel = this.panelViews[idx]

            switch (activePanel.sticky) {
                case 'TOP':
                    stickyTop.push([idx, activePanel.seq])
                    break
                case 'NONE':
                    sort.push([idx, activePanel.seq])
                    break
                case 'BOTTOM':
                    stickyBottom.push([idx, activePanel.seq])
                    // TODO Keep track of the PANEL insertion point
                    // if (activePanel.id === 'system-addPanel') {
                    //     addPanelSeq = idx
                    // }
                    break
            }
        }

        // Compare function to sort A..Z on second (1) item in array. Which is SEQ
        function compare (a:any, b:any) {
            if (a[1] > b[1]) return 1
            if (a[1] < b[1]) return -1
            return 0
            // return a[1] > b[1]
        }

        // Sort the `panelViews` for each (none) STICKY type group.
        stickyTop.sort(compare)
        sort.sort(compare)
        stickyBottom.sort(compare)
        idx = 0
        // this.panelViews = []

        // Rebuild the `panelViews` display order
        stickyTop.forEach((s:any) => {
            this.panelViews[s[0]].seq = idx
            idx += 1
        })
        sort.forEach((s:any) => {
            this.panelViews[s[0]].seq = idx
            idx += 1
        })
        stickyBottom.forEach((s:any) => {
            this.panelViews[s[0]].seq = idx
            idx += 1
        })

        this.redraw()
    }

    /**
     * Count panels
     * @returns Object with `total` panels and total panels `active`.
     */
    public countPanels ():{active:number, total:number} {
        return {active: this.panelViews.length, total: this.panels.length}
    }

    /**
     * Deactivate panels on screen and hide them for the user.
     * Except for 'STICKY' panels.
     * @param panels to deactivate, as in a list of panel ID's
     */
    public deactivatePanels (deactivatePanelLst:string[]) {
        let idx = 0
        let seqLst:string[] = []

        deactivatePanelLst.forEach((id:string) => {
            let panel:Panel = this.findPanelByID(id)

            // Check if panel is sticky.BOTTOM or sticky.TOP.
            // If so, SKIP deactivation.
            if (panel.sticky === 'NONE') {
                // Hide it from the DOM
                panel.el.classList.add('panel--inactive')
                // Administer it as not active
                panel.active = false
                // Mark as removed
                seqLst[panel.seq] = ""
            }
        })

        // Build current visible sequence list
        // Containing only STICKY panels.
        // Add keeping current SEQ flow.
        this.panels.forEach((panel:Panel) => {
            if (panel.active) {
                seqLst[panel.seq] = panel.id
            }
        })

        // Clean-up, reorder sequences
        for (idx = 0; idx < seqLst.length; idx += 1) {
            if (!seqLst[idx]) {
                seqLst.splice(idx, 1);
                idx -= 1 // One step back
            }
        }

        
        // Re-order sequences of visible panels, calculate possible new position
        this.panelViews = [] // Reset
        seqLst.forEach((id:string, idx:number) => {
            let panel:Panel = this.findPanelByID(id)
            panel.setSeqNo(idx)
            panel.active = true
            this.panelViews.push(panel)
            this.setPanelLayout(panel, idx, panel.currentState)
        })
        this.redraw()
    }

    /**
     * Dump the panel layout, as stored in the GRID, as a map of columns and rows.
     */
    public dumpGrid ():void {
        let idx:number
        let row = 0 // Start at GRID ROW 0
        let txt = "0: "

        console.log(`Grid layout (col:${this.dimension.columns}, row:${this.dimension.rows})`) // DumpGrid()

        for (idx = 0; idx < this.gridCount; idx += 1) {
            if (this.grid[idx].row != row) {
                // New row, print result to force new line
                console.log(txt) // DumpGrid()
                txt = this.grid[idx].row + ": " 
                row = this.grid[idx].row
            }
            txt += (this.grid[idx].panelID || "____") + " "
        }
        console.log(txt) // DumpGrid()

        console.log(this.panels) // DumpGrid()
        console.log(this.panelViews) // DumpGrid()
    }

    /**
     * Search through the PANEL list `panelViews`, to find a specific panel `id`.
     * @param id of the PANEL to search for
     * @returns Panel with `id`
     */
    public findPanelByID (id:string):Panel {
        if (id === undefined) {
            console.log(`findPanelByID:'${id}'NOT found!`)
        }
        return (<any>this.panels).find(function(panel:Panel) {
            return panel.id === id
        })
    }

    /**
     * Returns a list of panel ID's, matching a given `filter`.
     * If NO FILTER given, then ALL panels are selected.
     * 
     * @param PanelFilter with `active`, `exclude` properties.
     */
    public findPanels (filter?: PanelFilter):string[] {
        let list:string[] = []

        if (!filter) {
            // Default SHOW ALL PANELS (including inactive)
            this.panels.forEach((panel:Panel) => {
                list.push(panel.id)
            })
        } else {
            if (filter.hasOwnProperty('isActive')) {
                // Select on property `isActive`
                this.panels.forEach((panel:Panel) => {
                    if (filter.isActive === panel.active) {
                        list.push(panel.id)
                    }
                })
            } else {
                // Default SHOW ALL PANELS (including inactive)
                this.panels.forEach((panel:Panel) => {
                    list.push(panel.id)
                })
            }
            
            // EXCLUDE PART
            if (filter.hasOwnProperty('exclude')) {
                if (typeof filter.exclude === 'string' || filter.exclude instanceof String) {
                    // Build an array of one.
                    filter.exclude = [<string>filter.exclude]
                }

                filter.exclude!.forEach((id:string) => {
                    list.splice(list.indexOf(id), 1)
                })
            }
        }


        return list
    }

    /**
     * Returns an Object containing a list of available PANEL states
     * AND the currently ACTIVE state.
     * @param id of the PANEL to scan
     * @returns a Object of panel state properties, indicating the visibility of a (active) state.
     */
    public getAvailablePanelStates (id:string):{states:State[], active:State} {
        const panel:Panel = this.findPanelByID(id)!
        return { states:panel.availableStates, active:panel.currentState }
    }

    /**
     * Build a list of PANEL STATES, representing the current USER VIEW of panel usage.
     * @returns an Array of `Stateset`.
     */
    public getCurrentState ():StateSet[] {
        let stateSet:StateSet[] = []

        this.panelViews.forEach((panel:Panel) => {
            stateSet.push({
                hasData: panel.hasData,
                id: panel.id,
                semaphore: panel.semaphore,
                semaphoreType: panel.semaphoreType,
                seq: panel.seq,
                state: panel.currentState,
                title: panel.title
            })
        })

        return stateSet
    }

    /**
     * Move the visual location of the panel `id` to panel id `beforeId`.
     * @param id of the panel to move before panel with id `beforeId`.
     * @param beforeId is the new location of panel `id`
     */
    public movePanelBeforePanel (id:string, beforeId:string):void {
        let currentSeq:string[] = []
        let insert = 0
        let kill = 0

        this.panelViews.forEach((panel:Panel) => {
            currentSeq[panel.seq] = panel.id
            if (id === panel.id) {
                kill = panel.seq
            }
            if (beforeId === panel.id) {
                insert = panel.seq
            }
        })

        // Switch PANEL (render) SEQUENCE 
        currentSeq.splice(insert, 0, currentSeq[kill])
        if (insert < kill) {
            currentSeq.splice(kill + 1, 1)
        } else {
            currentSeq.splice(kill, 1)
        }

        currentSeq.forEach((id:string, idx:number) => {
            this.findPanelByID(id).setSeqNo(idx)
        })

        this.redraw()
    }

    /**
     * When PANEL are displayed and or changed in STATE. 
     * Then they will give a visual ping to give the USER an indication what is changed on screen.
     * @param panelLst of PANEL ID's to recieve a visual PING signal.
     */
    public pingPanels (panelLst:string | string[]):void {
        // We expect an array by default, but a single panel ID `string` is also excepted
        if (typeof panelLst === 'string' || panelLst instanceof String) {
            // Build an array of one.
            panelLst = [<string>panelLst]
        }

        panelLst.forEach(id => {
            // Blink panel
            this.findPanelByID(id).el.classList.add('panel--ping')
        })

        // Remove the CSS class `panel--ping)`, after 4 sec, 
        // because that is the CSS TRANSITION TIME.
        setTimeout( ((panelLst:string[]) => {
            return () => {
                panelLst.forEach(id => {
                    this.findPanelByID(id).el.classList.remove('panel--ping')
                })
            }
        })(panelLst), 4000)
    }

    /**
     * Set a visual focus to a SINGLE or a SET of panels.
     * And release the focus of previous focussed panel(s).
     * @param data "String", ["String"], Panel, [Panel]
     */
    public setFocusToPanel (data:any) {
        let panelEl:HTMLElement[] = []
        let focusOn = document.querySelectorAll('.panel--has-focus')

        // Remove existing foucusses
        if (focusOn.length) {
            focusOn.forEach( e => {
                e.classList.remove('panel--has-focus')
            })
        }

        // Check for Array property
        if( Object.prototype.toString.call( data ) === '[object Array]' ) {
            // Array, but what kind of data type STRING or PANEL object?
            if (typeof data[0] === 'string' || data[0] instanceof String) {
                // Array of Strings
                data.forEach((p:any) => {
                    panelEl.push(document.getElementById(<string>p)!)
                })
            } else {
                // Array of Panel objects
                data.forEach((p:Panel) => {
                    panelEl.push((<Panel>p).el)
                })
            }
        } else {
            // No Array, so a single item
            if (typeof data === 'string' || data instanceof String) {
                panelEl.push(document.getElementById(<string>data)!)
            } else {
                panelEl.push((<Panel>data).el)
            }
        }

        panelEl.forEach(e => {
            e.classList.add('panel--has-focus')
        })
    }

    /**
     * Calculate the new physical location of the `panel`,
     * depending on new sequence `seq` and or panel `state`.
     * @param panel to set the new layout properties
     * @param seq optional new panel location
     * @param state optional new panel state
     */
    public setPanelLayout (panel: Panel, seq?:number, state?:State) {
        const states:PanelStateSet = this.scanPanelStateDimensions(panel)

        let footerEl = panel.el.getElementsByClassName('panel__footer')!

        if (state) {
            panel.previousState = panel.currentState
            panel.currentState = state
        }
        panel.columnSize = states[panel.currentState].columnSize
        panel.rowSize = states[panel.currentState].rowSize

        if (seq !== undefined) {
            panel.setSeqNo(seq)
        }

        // Is el = `null` that PANEL `state` is not available (no HTML), so already hidden.
        !states['SUMMARY'].el || (states['SUMMARY'].el.hidden = true)
        !states['ENLARGED'].el || (states['ENLARGED'].el.hidden = true)
        !states['EXPANDED'].el || (states['EXPANDED'].el.hidden = true)

        // Then activate correct one
        // Only for 'MINIMAL' state we hide the FOOTER functionality
        // Footers ARE NOT MANDATORY and can be missing to allow more data in panel.
        if (panel.currentState.toLowerCase() !== 'minimal') {
            states[panel.currentState].el.hidden = false;
            if (footerEl.length > 0) {
                (<HTMLElement>panel.el.getElementsByClassName('panel__footer')[0]).hidden = false
            }
        } else {
            if (footerEl.length > 0) {
                (<HTMLElement>panel.el.getElementsByClassName('panel__footer')[0]).hidden = true
            }
        }

        // this.pingPanels(panel.id)
    }

    /**
     * CHANGE the panel fixed LOCATION on screen (dashboard).
     * It's possible to have multiple panels with the same STICKY type.
     * 'BOTTOM' - Place panel at the bottom of the panel list.
     * 'NONE' -  Place panel anywhere, except in 'BOTTOM'|'TOP' sub lists.
     * 'TOP' -  Place panel at the top of the panel list.
     * @param id of the PANEL object.
     * @param sticky type; 'BOTTOM'|'NONE'|'TOP'.
     */
    public setPanelToSticky (id:string, sticky:Sticky) {
        let panel = this.findPanelByID(id)

        switch (sticky) {
            case 'BOTTOM':
                // Show panel at the bottom of the dashboard, always visible
                // Use by 'Add panel' PANEL.
                break
            case 'NONE':
                // Show panel where the user, preset or system placed it.
                break
            case 'TOP':
                // Show panel at the top of the dashboard, always visible
                break
        }

        panel.sticky = sticky

        this.activatePanels([id])
    }

    /**
     * Shows a set of PANELS with a predefined STATE.
     * Inject them (or move them) to the position of the 'Add panel'-panel.
     * And preserve position of STICKY panels.
     * @param stateSet of panels with a predefined STATE 
     */
    public showPanelSet (stateSet:StateSet[]) {
        let checkSum = 0
        let checkSumCtrl = 0
        let sort:string[] = []

        // Check given `StateSet.seq` numbers
        // And build panel ID SORT list
        // TODO: put checksum on a different flow.
        stateSet.forEach((set:any, idx:number) => {
            checkSum += set.seq // `seq` number must start with 0.
            checkSumCtrl += idx // `idx` starts with 0.
            sort[set.seq] = set.id // Set default order given by stateSet.panel.seq
        })
        // Check checksums. This because of DEV USER defining the wrong `StateSet.seq`.
        // Which leads to unexpected errors.
        // `.seq` start at 0 and has sequential numbering 0, 1, 2, 3, ...
        if (checkSum !== checkSumCtrl) {
            console.error(`Preset has incorrect 'StateSet.seq' numbering. Start with 0 and sequential.`)
        }

        // Show panel set, adding them to the `panelViews` array.
        this.activatePanels(sort)
        
        // New panelViews sort
        sort = this.reorderPanelSeqStartingWith(sort)

        stateSet.forEach((set:any) => {
            let panel = this.findPanelByID(set.id)
            let panelState:PanelStateSet = this.scanPanelStateDimensions(panel)

            // Filter out STATES not supported by panel.
            // If so, keep current STATE.
            if (!(panelState[(<State>set.state)].columnSize === 0 && 
                panelState[(<State>set.state)].rowSize === 0)) {
                panel.previousState = panel.currentState
                panel.currentState = set.state
            }
        })
        
        sort.forEach((id:string, idx:number) => {
            let panel = this.findPanelByID(id)
            this.setPanelLayout(panel, idx, panel.currentState)
        })

        this.redraw()
    }

    /**
     * Redraw the panels in size and location (sequence).
     */
    public redraw () {
        // Allocate panels to the GRID
        this.mapPanelsToGrid() // You can add a sorter function as parameter.
        // Calculate panel size and location, according to the GRID
        this.sizePanelsToWindow()
    }

    /**
     * This function reorders the list of panels, putting a list of panels on top of the existing list.
     * @param newSort is a list of panel ID's to display at first, top left of the viewport.
     */
    public reorderPanelSeqStartingWith (newSort:string[]):string[] {
        let addPanelSeq = 0 // Default panel INSERT position
        let currentPanelSort:string[] = []
        let idx = 0
        let newPanelSort:string[] = [] // The array sequence is the new order
        let sort:any = []
        let stickyBottom:any = []
        let stickyTop:any = []

        // Walk through now ACTIVE (visible) panels.
        // Order them in (none) STICKY groups
        for (idx = 0; idx < this.panelViews.length; idx += 1) {
            const activePanel = this.panelViews[idx]

            switch (activePanel.sticky) {
                case 'TOP':
                    // idx:Current panel index
                    // activePanel.seq:the requested (original position)
                    stickyTop.push([idx, activePanel.seq])
                    break
                case 'NONE':
                    sort.push([idx, activePanel.seq])
                    break
                case 'BOTTOM':
                    stickyBottom.push([idx, activePanel.seq])
                    if (activePanel.id === 'system-addPanel') {
                        addPanelSeq = idx
                    }
                    break
            }
        }

        // Compare function to sort A..Z on second (1) item in array. Which is SEQ
        // The new order is the panel.SEQuence
        function compare (a:any, b:any) {
            if (a[1] > b[1]) return 1
            if (a[1] < b[1]) return -1
            return 0
            // return a[1] < b[1]
        }

        // Sort the `panelViews` for each (none) STICKY type group.
        stickyTop.sort(compare)
        sort.sort(compare)
        stickyBottom.sort(compare)
        idx = 0

        // Rebuild the `panelViews` display order
        stickyTop.forEach((s:any) => {
            this.panelViews[s[0]].seq = idx
            idx += 1
        })

        // The `sort` is influenced by the requested new sort order
        newSort.forEach((id:string) => {
            const panel = this.findPanelByID(id)
            // Be sure it's not a sticky one
            if (panel.sticky === 'NONE' && panel.active) {
                panel.seq = idx
                idx += 1
            }
        })
        // Add the remaining Panels, with their own sequence.
        sort.forEach((s:any) => {
            // Check if panel in view NOT part of the REORDER request
            if (newSort.indexOf(this.panelViews[s[0]].id) < 0) {
                // Not in the `newSort` array.
                this.panelViews[s[0]].seq = idx
                idx += 1
            }
        })

        stickyBottom.forEach((s:any) => {
            this.panelViews[s[0]].seq = idx
            idx += 1
        })
        
        newPanelSort = []
        this.panelViews.forEach((panel:Panel) => {
            newPanelSort[panel.seq] = panel.id
        })

        return newPanelSort
    }

    /**
     * Resizing the grid, because of changes in the view port can be dangerous.
     * Because some panels may claim more `data-panel-colums`, than actual available.
     * Say column grid is 4, but a panel needs 5.
     */
    public resizeGrid (dimension: GridSize)  {
        this.dimension = {columns: dimension.columns, rows: dimension.rows}
        this.grid = [] // TOFDO: Move to function setGridDimension

        // Set GRID dimension in columns and rows
        this.setGridDimension(dimension)
        // Map panel definition size (width as columns, height as rows), to internal GRID
        this.mapPanelsToGrid()
        // Calculate physical panel dimension, as seen on screen
        this.sizePanelsToWindow()
    }

    /**
     * Search for a part of a TEXT in the content of all available (incl. inactive) panels.
     * Open these panels and put them on top of the panel view.
     * @param searchFor this TEXT in the page content
     */
    public searchFor (searchFor:string) {
        let textNodes:Node[]
        let panelSort:string[] = []
        let zoom:string[] = []

        /**
         * Find and select TEXTNODES where text `searchFor` exists
         * @param el the parent ELEMENT to walk through it's TEXTNODES children.
         */
        function textNodesUnder (el:HTMLElement):Node[] {
            let n:Node
            let textInNodes:Node[] = []
            let walk:TreeWalker = document.createTreeWalker(
                el, // Start here as parent
                NodeFilter.SHOW_TEXT, // Filter for 
                <any>{ acceptNode: function(node:any) {
                        // Logic to determine whether to accept, reject or skip node
                        // In this case, only accept nodes that have content
                        // other than whitespace
                        if (node.data.toLowerCase().search(searchFor.toLowerCase()) >= 0) {
                            return NodeFilter.FILTER_ACCEPT;
                        }
                    }
                },
                false
            )

            // Loop through all `textNodes` and their TEXT content.
            while (n = walk.nextNode()!) textInNodes.push(n)

            return textInNodes
        }

        // Start scanning for content
        textNodes = textNodesUnder(document.getElementById('wrapperPanels')!)
        if (textNodes.length === 0) {
            alert (`No search results for '${searchFor}'.`)
        } else {
            // Search for panel where textnode belongs to
            textNodes.forEach((panel:Node) => {
                let id:string

                while (!((panel as HTMLElement).classList && (panel as HTMLElement).classList.contains('panel'))) {
                    panel = panel.parentElement as Node
                }

                // Check on duplicates! TODO: Use set() but got Typescript compiler problems 'es2015' ...
                id = (panel as HTMLElement).id
                if (zoom.indexOf(id) < 0) {
                    zoom.push(id)
                    panelSort.push(id)
                    this.activatePanels([id])
                }
            })

            this.reorderPanelSeqStartingWith(panelSort).forEach((id:string, idx:number) => {
                const panel:Panel = this.findPanelByID(id)
    
                if (zoom.indexOf(panel.id) >= 0) {
                    this.setPanelLayout (panel, idx, 'SUMMARY')
                } else {
                    this.setPanelLayout (panel, idx, 'MINIMAL')
                }
            })

            this.mapPanelsToGrid()
            this.sizePanelsToWindow()
        }
    }

    /**
     * Change the visual display state of a single PANEL.
     * We support 4 states 'MINIMAL'|'SYMMARY'|'EXPANDED'|'ENLARGED'
     * - 'MINIMAL', only title an options are shown
     * - 'SYMMARY', displays a short, expresive summary of the data
     * - 'EXPANDED'. expands the summary info a more expresive and detailed format
     * - 'ENLARGED', for indept detailed information analysis
     * @param Object containing `Panel ID` and the new panel `state`.
     */
    public setPanelState ({ id, state }: { id:string; state:State; }):void {
        // panel.seq is `undefined`, keep the same sequence number
        this.setPanelLayout(this.findPanelByID(id), undefined, state)
        // Reset GRID and map all panel dimension to the grid, in order specified by `panel.seq`.
        this.mapPanelsToGrid() // You can add a sorter function as parameter.
        this.sizePanelsToWindow()
    }

    /**
     * Sort panels (using left to right) on panel `Title`.
     * Keep the sequence of STICKY panels the same
     */
    public sortOnTitle () {
        let seq = 0
        // The set of PANEL will be sorted on alphabet A..Z
        let sort:any = []
        // These STICKY panels, are untouched in sequence and keep their position.
        let stickyBottom:any = []
        let stickyTop:any = [] 

        // Build comparison TITLE data list
        // Skip SORT for STICKY panels.
        this.panelViews.forEach((panel:Panel, idx:number) => {
            switch (panel.sticky) {
                case 'TOP':
                    stickyTop.push([idx, panel.title])
                    break
                case 'NONE':
                    sort.push([idx, panel.title])
                    break
                case 'BOTTOM':
                    stickyBottom.push([idx, panel.title])
                    break
            }
        })

        // Compare function to sort A..Z on second (1) item in array. Which is TITLE.
        function compare (a:any, b:any) {
            return a[1].localeCompare(b[1]);
        }

        // Sort TITLE, using the sort function `compare`.
        sort.sort(compare)

        // Change the panel sequence, BUT not the the panel dimensions.
        // Start with sticky.TOP first
        stickyTop.forEach((item:any[]) => {
            this.panelViews[item[0]].setSeqNo(seq)
            seq += 1
        })
        // Then do NONE sticky panels
        sort.forEach((item:any[]) => {
            this.panelViews[item[0]].setSeqNo(seq)
            seq += 1
        })
        // Do Sticky.BOTTOM panels
        stickyBottom.forEach((item:any[]) => {
            this.panelViews[item[0]].setSeqNo(seq)
            seq += 1
        })

        // Because panel.seq hase been changed. NOT panel SIZE, but laction on viewport
        this.mapPanelsToGrid()
        this.sizePanelsToWindow()
    }

    /**
     * Restore all PANELS to it's default state and order.
     * The defaults are defined as specifiied in the originally loaded HTML DOM.
     * See attributes <div class="panel" data-panel-default-state="..." data-panel-seq="..."
     * - `data-panel-default-state`, one of 'MINIMAL'|'SYMMARY'|'EXPANDED'|'ENLARGED'
     * - `data-panel-seq`, panel render sequence number. Starts at 1 and is sequential
     */
    public restoreAllPanelsToDefaultState ():void {
        let sort:string[] = []

        // Reset current PANEL VIEWS
        this.deactivatePanels(this.findPanels({isActive: true})) // Currently visisble on screen

        this.panels.forEach((panel:Panel) => {
            if (panel.defaultActive) {
                panel.previousState = panel.currentState
                panel.currentState = <State>panel.defaultState.toUpperCase()
                panel.active = panel.defaultActive
                panel.seq = panel.defaultSeq
                this.setPanelLayout(panel)
                // Add to active `panelViews`.
                this.activatePanels([panel.id])
                sort[panel.defaultSeq] = panel.id
            }
        })

        this.redraw()
    }

    /**
     * Set the status of all PANELS to a given `state`.
     * And change the layout accordingly.
     * @param state one of 'MINIMAL'|'SYMMARY'|'EXPANDED'|'ENLARGED'
     */
    public setStatusOfAllPanelsTo (state:State):void {
        const maxPanels = this.panelViews.length

        let idx:number

        // TODO: is simple but invokes a lot of panel restructuring
        // Because ever single panel change will redo calculation of all panels.
        // Meaning 15 panels will invoke 15 x 15 updates.
        for (idx = 0; idx < maxPanels; idx +=1) {
            this.setPanelState ({ id:this.panelViews[idx]!.id, state:state })
        }
    }
}

export {Panels}
