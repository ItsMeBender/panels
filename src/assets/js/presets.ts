import {  } from "../../../typings/presets";
import { PresetFilter, PresetSet, StateSet } from "../../../typings/presets";

/**
 * PRESETS are PANEL configurations (state, location, active) grouped under a unique label.
 * It allows the user to quickly access certain panels and panel configurations.
 * The system provides some default panel presets.
 * But the USER can add new presets or modify existing presents. 
 * This means that the list of presets can grow. 
 * Where the need arises for the management of presets.
 * - There is even a thought of sharing presents between users (organisations).
 * 
 * Functions:
 * - A list of presets
 * - Each preset has a LABEL, a DESCRIPTION, all related (involved) panels
 * - System declared presets can not be edited, but used as a templet for user presets.
 * - User can add new presets
 * - User can modify, deleted own presets (can be a problem when sharing)
 * - Each preset has a default PANEL STATE, but provides shortcuts to force a certain panel STATE.
 * - Text search in presets, is the most basic UI for presets selection. Besides an alphabetical list.
 */

let activeRole = 'PODOTHERAPIST'
let hasData:boolean // TODO: Improve JS

 // A single data PRESET object.
class Preset {
    callBack:Function
    description:string
    el: HTMLElement
    label:string
    id:string
    panelSet:StateSet[] // List of panels involved
    role:string

    constructor (id:string, label:string, description:string, role:string, panels:StateSet[], callBack:Function) {
        this.callBack = callBack
        this.description = description
        this.label = label
        this.panelSet = panels
        this.role = role
        this.id = id
        this.el = document.createElement('LI')
    }
}

// A CLASS to handle the list of USER and SYTEM PRESETS.
class Presets {
    presets:Preset[]

    constructor () {
        this.presets = []
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE methods
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Build a PRESET LIST element and add this one to th `UL` container.
     * @param presetUl is the container for all `LI` list elements
     */
    private createPresetLst (presetUl:HTMLElement) {
        this.presets.forEach((preset:Preset) => {
            let count = this.getPanelLst(preset.label).length
            let panelNames = ''
            let panelSemaphore = ''
            let panelSemaphoreClasses = ''
            let panelSemaphoreIcon = ''
            let presetLi:HTMLElement = preset.el
            let regex = /,/gi
            let userNav = ''
            let userStates = ''
            
            // Different options when it's an SYSTEM preset
            if (preset.role === "USER") {
                userNav = `
                    <button class="btn--panelstate btn__action--update" type="button" title="Update current view under this preset">Aanpassen</button>
                    <button class="btn--panelstate btn__action--delete" type="button" title="Verwijder preset">Verwijder</button>
                    `
                }
                
                userStates = `
                <button class="btn--panelstate btn__showstate--minimal" type="button" title="Minimize panels">minimaal</button>
                <button class="btn--panelstate btn__showstate--summary" type="button" title="Show panel summaries">overzicht</button>
                <button class="btn--panelstate btn__showstate--expanded" type="button" title="Show panels as data">tabel</button>
                <button class="btn--panelstate btn__showstate--enlarged" type="button" title="Show panels as other">grafiek</button>
                <button class="btn--panelstate btn__showstate--closed" type="button" title="Close all panels in this preset">Sluit</button>
            `
            
            // Different options if there are NO panels involved (Close All)
            if (count === 0 ) {
                userStates = '' 
            } else if (count === 1) {
                // Only one panel as preset
                // Name not needed, because it's the same already displayed as TITLE.
                panelNames = ''
                // Activate PANEL SEMAPHORE
                switch (preset.panelSet[0].semaphore) {
                    case 'NONE':
                        panelSemaphoreClasses = ''
                        break;
                    case 'LOW':
                        panelSemaphoreClasses = 'attention_low'
                        break;
                    case 'MEDIUM':
                        panelSemaphoreClasses = 'attention_medium'
                        break;
                    case 'HIGH':
                        panelSemaphoreClasses = 'attention_high'
                        break;
                }

                // Activate PANEL SEMAPHORE
                switch (preset.panelSet[0].semaphoreType) {
                    case 'NONE':
                        panelSemaphoreIcon = ''
                        break;
                    case 'DOWN':
                        panelSemaphoreIcon = '<svg class="icon primary_color"><use xlink:href="img/iconset.svg#arrow-downward"></svg>'
                        break;
                    case 'EQUAL':
                        panelSemaphoreIcon = '<svg class="icon primary_color"><use xlink:href="img/iconset.svg#arrow-stop"></svg>'
                        break;
                    case 'UP':
                        panelSemaphoreIcon = '<svg class="icon primary_color"><use xlink:href="img/iconset.svg#arrow-upward"></svg>'
                        break;
                }                
            } else {
                // More than one panel involved, show panel titles.
                panelNames = this.getPanelLst(preset.label).toString().replace(regex,', ') + ` - (${count})`
            }
            
            // Check if PRESET PANELS containe any data,
            // If NOT, mark them as empty to avoid clicking on EMPTY items
            hasData = false
            preset.panelSet.forEach((state:StateSet) => {
                hasData = state.hasData || hasData
            })

            // Check if panels, selected by preset do have data in them.
            // So that if user decides to select preset, something can be shown.
            if (hasData) {
                presetLi.setAttribute('class', 'list__item')

                // A preset can have multiple panels (count).
                // If so, preset is NOT a SINGLE panel, so no SEMAPHORE can be shown.
                // TODO: Show semaphore per panel subscription (tag line ... 'panelNames')
                if (count > 1) {
                    panelSemaphore = `
                        <div class="list__semaphore">&nbsp;</div>
                    `
                } else {
                    panelSemaphore = `
                        <div class="list__semaphore ${panelSemaphoreClasses}">${panelSemaphoreIcon}</div>
                    `
                }
            } else {
                // Preset contains no data, so give visual clue, that clicking leads to nothing.
                presetLi.setAttribute('class', 'list__item preset--nodata')
                // Semaphore is EMPTY, because there is no data
                panelSemaphore = `
                    <div class="list__semaphore">&nbsp;</div>
                `
            }
            presetLi.dataset.presetId = preset.id
            
            presetLi.innerHTML = `
            <div class="list__column">
                ${panelSemaphore}
            </div>
            <div class="list__preset">
                <div class="list__info">
                    <div class="preset__label">${preset.label} <span class="preset__description">- ${preset.description}</span></div>
                    <div class="preset__panels">${panelNames}</div>
                </div>
                <div class="list__states">
                ${userStates}
                </div>
                <div class="list__nav">
                ${userNav}
                </div
            </div>
            `

            presetUl.appendChild(presetLi) // As last elemt so it should
        })
    }

    /**
     * Delete a PRESET.
     * A. When a type `USER` preset then delete
     * B. When a type `SYSTEM` preset then HIDE from user list
     * C. When a type is a ROLE like `PODOTHERAPIST` | `DIETICIAN` | `PHYSIOTHERAPIST`
     * @param id of the PRESET to be deleted
     */
    private delete (id:string) {
        let idx:number
        let max = this.presets.length

        for (idx = 0; idx < max; idx += 1) {
            if (this.presets[idx].id === id) {
                // DEMO CODE: Don't delete ROLE presets, like 'Podotherapist'.

                if (this.presets[idx].role === 'USER') {
                    this.presets.splice(idx, 1)
                }
                // Item DELETED so abort
                break
            }
        }
        this.showSystemPresets(false)
    }

    /**
     * Return PRESET object, identified by preset `id`.
     * As in element attribute `data=preset-id`, to identify USER clicks.
     * @param id of the PRESET (element attribute `data-preset-id`)
     */
    private getPresetById (id:string):Preset {
        let retval:any

        this.presets.forEach((preset:Preset) => {
            if (preset.id === id) {
                retval = preset
            }
        })
        return retval || null
    }

    /**
     * Return a list of PANELS ID's related to a PRESET called `label`.
     * @param label of the PRESET to get the PANEL list
     */
    private getPanelLst (label:string):string[] {
        let list:string[] = []

        this.presets.forEach((preset:Preset) => {
            if (preset.label === label) {
                preset.panelSet.forEach((set:StateSet) => {
                    list.push(set.title!)
                })
            }
        })
        return list
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // PUBLIC methods
    //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Add a PRESET to the internal PRESET DATA structure.
     * Which will be used to build the PRESET UI.
     * @param Object with a single PRESETS data definition
     * @returns a unique preset ID.
     */
    public add (preset:PresetSet):string {
        let id = this.presets.length.toString()

        // Declare SORT function
        function compare (a:any, b:any):any {
            return a.label.localeCompare(b.label)
        }

        // Add to the internal PRESET administrative LIST.
        this.presets.push(new Preset(id, preset.label, preset.description, preset.role, preset.stateSet, preset.callBack))
        // Sort TITLE, using the sort function `compare`.
        this.presets.sort(compare)

        return id
    }

    /**
     * Physically (as in HTML DOM-tree) build the complete HTML list of SYSTEM
     * and USER PRESETS. Stored in the DIV container `presetContainer`.
     */
    public buildPresetList () {
        let presetUl:HTMLElement = document.getElementById('presetList')!

        // Check if this function `buildPresetList` already has been calculated
        // And prepared EVENTS and stuff.
        if (presetUl) {
            let presetEl = presetUl.firstChild!
            // Old list exists, so update UL > LI by removing OLD `LI`-elements
            while( presetEl ){
                presetUl.removeChild(presetEl);
                presetEl = presetUl.firstChild!
            }
            // Show updated list
            this.createPresetLst(presetUl)
        } else {
            let presetWrapper:HTMLElement = document.getElementById('presetWrapper')!
            
            presetUl = document.createElement('UL')
            
            // Set container <UL> attributes
            presetUl.setAttribute('class', 'list__container')
            presetUl.setAttribute('id', 'presetList')
            // Set GLOBAL Event catcher
            presetUl.addEventListener('click', (e:any) => {
                let isButtonClose:boolean
                let isButtonDelete:boolean = false
                let isButtonEnlarged:boolean
                let isButtonExpanded:boolean
                let isButtonMinimal:boolean
                let isButtonSummary:boolean
                let isButtonUpdate:boolean = false
                let preset:Preset
                let state:string = ''
                let target = e.target!
                
                // Check if and which button was clicked on Preset ROW.
                if (target.nodeName.toUpperCase() === 'BUTTON') {
                    isButtonClose = target.classList.contains('btn__showstate--closed')
                    isButtonDelete = target.classList.contains('btn__action--delete')
                    isButtonEnlarged = target.classList.contains('btn__showstate--enlarged')
                    isButtonExpanded = target.classList.contains('btn__showstate--expanded')
                    isButtonMinimal = target.classList.contains('btn__showstate--minimal')
                    isButtonSummary = target.classList.contains('btn__showstate--summary')
                    isButtonUpdate = target.classList.contains('btn__action--update')

                    switch (true) {
                        case isButtonMinimal:
                            state = 'MINIMAL'
                            break;
                        case isButtonSummary:
                            state = 'SUMMARY'
                            break;
                        case isButtonExpanded:
                            state = 'EXPANDED'
                            break;
                        case isButtonEnlarged:
                            state = 'ENLARGED'
                            break;
                        case isButtonClose:
                            // Work-a-round to make 'CLOSE' button works (NOT SO NICE SOLUTION)
                            state = 'CLOSE'
                            break;
                        default:
                            state = ''
                    }
                } // End PRESET UL `click` listener
                                
                // Get row PRESET.ID when clicked.
                while (!(target as HTMLElement).classList.contains('list__item')) {
                    target = target.parentElement
                }
                preset = this.getPresetById(target.dataset.presetId)
                                        
                if (isButtonUpdate || isButtonDelete) {
                    if (isButtonDelete) {
                        // Delete PRESET from HTML DOM
                        // JUst DEMO code (can not physically delete default system preset)
                        if (preset.label !== 'Podotherapist') {
                            target.parentElement.removeChild(target)
                        }
                        // Remove from list
                        this.delete(preset.id)
                    } else {
                        // UPDATE TODO NOT SUPPORTED YET
                    }
                } else {
                    preset.callBack(state)
                }
            })
            
            this.createPresetLst(presetUl)
            
            presetWrapper.appendChild(presetUl)
        }
    }

    /**
     * Returns a preset, exactly matching given panel id's.
     * @param filter with panel ID's to search for.
     */
    public checkIfPresetExists (filter: PresetFilter):Preset {
        let list:Preset
        let panelCount:number

        // We expect an array by default, but a single panel ID `string` is also excepted
        if (typeof filter.panelIDs === 'string' || filter.panelIDs instanceof String) {
            // Build an array of one.
            filter.panelIDs = [<string>filter.panelIDs]
        }
        
        panelCount = filter.panelIDs!.length

        list = (<any>this.presets).find((preset:Preset) => {
            if (preset.panelSet.length === panelCount) {
                let panelSetIsEqual = true;

                // Walk through ALL panels in the panelset
                // Bcause all filter panels must match
                (<any>preset.panelSet).forEach((set:StateSet) => {
                    if (panelSetIsEqual) {
                        // Still matching panels found
                        // Check for the next one.
                        if (!(<any>filter.panelIDs!).find((id:string) => set.id === id)) {
                            // Diffrence in panel set found, so abort
                            panelSetIsEqual = false
                        }
                    }
                })
                return panelSetIsEqual
            
            } else {
                // Number of panels aren't equal, so next preset
                return false
            }
        })

        return list
    }

    /**
     * Returns a list of preset's, matching a given `filter`.
     * If NO FILTER given, then ALL presets are selected.
     * @param PanelFilter with panel.active option
     */
    public findPresets (filter?: PresetFilter):Preset[] {
        let list:Preset[] = []

        // Check if a filter has been provided
        if(!filter) {
            // Default FILTER ALL PRESETS
            this.presets.forEach((preset:Preset) => {
                list.push(preset)
            })
        } else {
            // Filter provided, check type, then execute
            if (filter.panelIDs) {
                list = this.presets.filter((preset:Preset) => {
                    return (<any>preset.panelSet).find((set:StateSet) => {
                        return (<any>filter.panelIDs!).find((id:string) => {
                            if (set.id === id) {
                                // Preset located, where PANEL ID relates to
                                // console.log(preset)
                                return true
                            }
                        })
                    })
                })
            }
        }

        return list
    }

    /**
     * Returns a PRESET identified by `id`.
     * @param id of the retruned preset
     */
    public get (id:string):Preset {
        return (<any>this.presets).find(function(preset:Preset) {
            return preset.id === id
        })
    }

    /**
     * Return an Array of PANEL ID's related to this PRESET (StateSet).
     * @param data the ID or StateSet of a preset.
     */
    public getPanelIds (data:string|StateSet[]):string[] {
        let panelIds:string[] = []

        if (typeof data === 'string' || data instanceof String) {
            this.get(<string>data).panelSet.forEach((set:StateSet) => {
                panelIds.push(set.id!)
            })
        } else {
            data.forEach((set:StateSet) => {
                panelIds.push(set.id!)
            })
        }
        return panelIds
    }

    /**
     * Set the role of the user.
     * Used to filter PRESETS for specific USER ROLE: 'DIETISIAN', 'POH', etc.'
     * @param role of the user
     */
    public setRole (role:string):void {
        activeRole = role.toUpperCase()
    }

    /**
     * All available PANELS are per default SYSTEM or USER panels.
     * Giving the user access to all kinds of available data sets.
     * Whereby the user can combine them into PERSONAL PRESETS (role='USER')
     * And there are also functional ROLE related presets (role=`PODOTHERAPIST`|`DIETICIAN`|`PHYSIOTHERAPIST`)
     * This function SHOW/HIDES these SYSTEM presets.
     * @param visible is true, then show also SYSTEM presents otherwise only PERSONAL (BUTNEVER 'NOTE" presets)
     */
    public showSystemPresets (visible = true) {
        this.presets.forEach((set:Preset) => {
            // When writing NOTES a preset is created with the panel DASHBOARD view on that moment.
            // If it's not an existing preset, IT WILL BE a NOTE presets.
            // These NOTE PRESETS are not shown to the USER in the preset list.
            if (set.role === 'NOTE') {
                set.el!.classList.add('hideobj')
            } else if (set.role === 'SYSTEM' ||
                (set.role === 'DIETICIAN' && set.role !== activeRole) || 
                (set.role === 'PODOTHERAPIST' && set.role !== activeRole) || 
                (set.role === 'POH' && set.role !== activeRole) || 
                (set.role === 'PHYSIOTHERAPIST' && set.role !== activeRole)
                ) {
                // Using a CSS Class, because the `hidden` attribute doesn't wotk for `flex`.
                if (visible) {
                    set.el!.classList.remove('hideobj')
                } else {
                    set.el!.classList.add('hideobj')
                }
            }
        })
    }

}

export {Preset}
export {Presets}
