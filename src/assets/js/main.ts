import { Panels } from "./panels.js"; // Because it is a JS Class
import { Presets } from "./presets.js";  // Because it is a JS Class
import { Notes } from "./notes.js";  // Because it is a JS Class
import { PresetSet, StateSet, PresetFilter } from "../../../typings/presets";
import { State } from "../../../typings/panels";

// //////////////////////////////////////////////////////////////////////

// HOW TO WORK, RUN THIS PROJECT
// Start three VCode terminals; compile, webserver, build tool
//  1. tsc --watch
//  2. npm run server
//     http://127.0.0.1:8080/?role=DIETICIAN
//  3. npm start
//  4. /index.html?role=DIETICIAN
//     DIETICIAN | PHYSIOTHERAPIST | PODOTHERAPIST (default) | POH | BLANK (no role)

// //////////////////////////////////////////////////////////////////////

// For build (manually) preset sets
let fixTS:PresetSet
let fixTSDiabetes:PresetSet
let fixTSPhysiotherapist:PresetSet
let fixTSPOH:PresetSet // PV Monitor preset
let fixTSPOHSecondLine:PresetSet // Second-Line data; Thuiszorg, social kaart ...
let fixTSPodotherapist:PresetSet
// Row minimum height 32px (the size of only showing only a panel header)
// Currently each panel has a `summary` height of 4 rows, so 10 panels under each other
let gridRow = 10

// Columns 12 on wide screen give 6 columns, with a panel column width of 2
// The CALL will invoke automatically the PANEL lay-out, based on `columns` and `rows`.
// Using the already panel setup in the DOM tree (as demo).
let panels = new Panels({columns:12, rows:gridRow * 4}) // 4 panel height `summary`
// Initiate the preset module
let presets = new Presets()
// Initiate the notes module
let notes = new Notes()

let biometricsPresetId = ''
let contactPresetId = ''
let date30DaysPresetId = ''
let date100DaysPresetId = ''

/**
 * Add the `click` OPEN MENU EVENT to the menu icons, tOP RIGHT of the panel.
 * It will build the menu, based on the available PANEL states.
 */
function addPanelMenuEventListeners ():void {
    let idx:number
    let optionsDivList:HTMLCollection = document.getElementsByClassName('header__options')

    for (idx = 0; idx < optionsDivList.length; idx += 1) {
        const menuIcon = optionsDivList[idx].firstChild! // Currently - Only Icon

        menuIcon.addEventListener('click', (e => { return (e:Event) => {
            let svgEl:HTMLElement = <HTMLElement>e.target!
            let rect:any = svgEl.getBoundingClientRect()

            // Find parent panels
            while (!svgEl.classList.contains('panel')) {
                svgEl = svgEl.parentElement!
            }

            buildMenu ({panelId:svgEl.id, absX:rect.left - 100 + 24, absY:rect.top + window.scrollY})
        }})())

    }
}

/**
 * Build a dialog form (modeless, blocking all other interaction element)
 * @param dialog the form as a HTML string. 
 */
function buildModelessLayer ({dialog}:{dialog:string}) {
    const body = document.getElementsByTagName('BODY')![0]

    let divOverlay = document.createElement('DIV')

    divOverlay.setAttribute('id', 'overlayModelessDialog')
    divOverlay.setAttribute('class', 'overlay')
    divOverlay.innerHTML = dialog

    body.appendChild(divOverlay)
}

/**
 * Build HTML-DOM to support an element to copy the patient dashboard, current view URI.
 */
function buildCopyLinkDialog () {
    let dialog = ''

    dialog = `
        <form id="formCopyLinkPreset" class="overlay__form">
            <h2>Deel</h2>
            <p>Kopieer pati&euml;nt dashboard link</p>
            <div class="uri">
                <input class="field__uri" name="dashboardUri" type="text" readonly="" size="45" value="https://dashboard.portavita.net/BH54ioTTr56B">
                <button id="btnCopyUri" type="button">Kopieer</button> 
            </div>
            <br>
            <p>Deze link kunt u in een document of samenvatting opnemen.<br>
            En daarmee verwijst u naar dit paneel overzicht (preset).</p>
            <p>Uw collega is dan instaat naar het zelfde overzicht te kijken als u.</p>
            <button id="btnCancelUri" type="button">Annuleer</button> 
        </form>
    `
    buildModelessLayer({dialog: dialog})
}

/**
 * Build HTML-DOM to support a FORM to enter "Save preset as" properties.
 * @param stateSet to be shown as data in the FORM.
 */
function buildSaveAsDialog (stateSet:StateSet[]) {
    let dialog = ''
    let panelNames = ''

    stateSet.forEach((set:StateSet) =>{
        panelNames += panelNames ? `, ${set.title}` : set.title
    })

    dialog = `
        <form id="formSavePreset" class="overlay__form">
            <h2>Opslaan preset</h2>
            <p>Sla het huidige overzicht en opmaak op, in jouw persoonlijke preset lijst.</p>
            <label for="GET-name">Naam preset</label><br>
            <input name="presetName" type="text" placeholder="Unique preset name"><br>
            <label for="GET-name">Korte beschrijving van de preset (reden)</label><br>
            <input name="presetDescription" type="text" placeholder="Preset omschrijving"><br>
            <br>
            <h3>Deze preset bevat de volgende panelen (${stateSet.length})</h3>
            <p>${panelNames}</p>
            <br>
            <button id="btnSavePresetAs" type="button">Bewaar</button> 
            <button id="btnCancelPreset" type="button">Annuleer</button> 
        </form>
    `
    buildModelessLayer({dialog: dialog})
}

/**
 * Build the HTML PANEL menu <DIV>, to set the available PANEL `state` by user.
 * By default a 'close' panel button is available.
 * @param panelId of the panel to attach the menu to
 * @returns the menu as an HTML-Element
 */
function buildMenu ( {panelId, absX, absY}: {panelId:string; absX:number; absY:number;} ):HTMLElement {
    const wrapperPanelsEl:HTMLElement = document.getElementById('wrapperPanels')!
    const menuItems = [
        {state:'MINIMAL', label:'Minimaal'},
        {state:'SUMMARY', label:'Overzicht'},
        {state:'EXPANDED', label:'Tabel'},
        {state:'ENLARGED', label:'Grafiek'},
        {action:'ADDNOTE', label:'Notitie'},
        {action:'PINPANEL', label:'Vast pinnen'}
    ]
    const statesLst = panels.getAvailablePanelStates(panelId)
    const specialMenus:any = {
        'imploded-biometrics': [
            {state:'MINIMAL', label:'Minimaal'},
            {state:'SUMMARY', label:'Overzicht'},
            {state:'EXPANDED', label:'Infotainment'},
            {state:'ENLARGED', label:'-'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
        'podo-automatischBehandeladvies': [
            {state:'MINIMAL', label:'Minimaal'},
            {state:'SUMMARY', label:'Overzicht'},
            {state:'EXPANDED', label:'Details'},
            {state:'ENLARGED', label:'-'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
        'podo-fundusControlLast': [
            {state:'MINIMAL', label:'Minimaal'},
            {state:'SUMMARY', label:'Overzicht'},
            {state:'EXPANDED', label:'Details'},
            {state:'ENLARGED', label:'-'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
        'zib-patient': [
            {state:'MINIMAL', label:'Minimaal'},
            {state:'SUMMARY', label:'Overzicht'},
            {state:'EXPANDED', label:'Details'},
            {state:'ENLARGED', label:'-'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
        'dashEpisodes': [
            {state:'MINIMAL', label:'Minimaal'},
            {state:'SUMMARY', label:'Overzicht'},
            {state:'EXPANDED', label:'Tabel'},
            {state:'ENLARGED', label:'Tijdslijn'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
        'fysio-CCQ': [
            {state:'MINIMAL', label:'Minimaal'},
            {state:'SUMMARY', label:'Overzicht'},
            {state:'EXPANDED', label:'Vraagstelling'},
            {state:'ENLARGED', label:'-'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
        'appl-notes': [
            {state:'MINIMAL', label:'Minimaal'},
            {state:'SUMMARY', label:'Overzicht'},
            {state:'EXPANDED', label:'Notities'},
            {state:'ENLARGED', label:'-'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
        'system-addNotes': [
            {state:'MINIMAL', label:'Minimaal'},
            {state:'SUMMARY', label:'Add'},
            {state:'EXPANDED', label:'-'},
            {state:'ENLARGED', label:'-'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
        'system-addPanel': [
            {state:'MINIMAL', label:'Minimal'},
            {state:'SUMMARY', label:'Search'},
            {state:'EXPANDED', label:'-'},
            {state:'ENLARGED', label:'-'},
            {action:'ADDNOTE', label:'Add note'},
            {action:'PINPANEL', label:'Pin top'}
        ],
    }

    let menuTemplate = []
    let menuDiv = document.createElement('DIV')
    let menuUl = document.createElement('UL')
    let menuLi:any

    menuDiv.appendChild(menuUl)
    // Build HTML menu structure
    menuDiv.setAttribute('class', 'menu__container')
    menuDiv.setAttribute('style', `left:${absX}px;top:${absY}px;`)
    
    // Replace default menu a specialized one
    if (specialMenus.hasOwnProperty(panelId)) {
        menuTemplate = specialMenus[panelId]
    } else {
        // Use deafult one
        menuTemplate = menuItems
    }

    // Menu items based on PANEL STATES
    menuTemplate.forEach((item:any) => {
        menuLi = document.createElement('LI')

        // Check if STATE is supported
        if (statesLst.states.indexOf((<State>item.state)) >= 0) {
            menuLi.dataset.state = item.state

            // Check if supported state is ACTIVE
            if (statesLst.active === (<State>item.state)) {
                menuLi.setAttribute('class', 'menu__item--active')
            }

            menuLi.appendChild(document.createTextNode(item.label))
            menuLi.addEventListener('click', ((id:string, state:State, menu:HTMLElement) => { return (e:Event) => {
                panels.setPanelState({id:id, state:state})
                // Kill menu DIV container
                menu.parentElement!.removeChild(menu)
            }})(panelId, <State>item.state, menuDiv))
            // Add menu item to MENU container
            menuUl.appendChild(menuLi)
        } else {

            switch (item.action) {
                case 'ADDNOTE':
                    menuLi.appendChild(document.createTextNode(item.label))
                    menuLi.addEventListener('click', ((id:string, state:State, menu:HTMLElement) => { return (e:Event) => {
                        console.log("TODO: Add Note")
                        // Kill menu DIV container
                        menu.parentElement!.removeChild(menu)
                    }})(panelId, <State>item.state, menuDiv))
                    // Add menu item to MENU container
                    menuUl.appendChild(menuLi)
                    break

                case 'PINPANEL':
                    break

                default:
                    console.log(`Handle menu option '${item.state}'.`)
            }

        }
    })

    // Menu item CLOSE option
    menuLi = document.createElement('LI')
    menuLi.appendChild(document.createTextNode('Close'))
    menuLi.addEventListener('click', ((id:string, menu:HTMLElement) => { return (e:Event) => {
        // Hide for user / panel interaction
        panels.deactivatePanels([id])

        // Update NOTES, Preset button label.
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()

        // Kill menu DIV container
        menu.parentElement!.removeChild(menu)
    }})(panelId, menuDiv))
    // Add menu item to MENU container
    menuUl.appendChild(menuLi)

    wrapperPanelsEl.parentElement!.appendChild(menuDiv) // As last element so it should

    return menuDiv
}

/**
 * Analyse the URL and return an object of parameters and values.
 * EXAMPLE: x.html?name1=value1&name2=value2 
 */
function getURLProperties () {
    let properties:any = {}
    let uri = window.location.toString().split('?')
    let uriProps
    if (uri.length > 1) {
        uriProps = uri[1].split('&')
        uriProps.forEach((prop:string) => {
            const a:string[] = prop.split('=')
            properties[a[0]] = <string>a[1]
        })
    }
    return properties
}

/**
 * Prepare PANELS for DRAG and DROP functionality.
 * Give the USER the ability to organize panels the way they prefer.
 * NOTE: There is a differnece between CHOME and FF in the way event are handled.
 */
function prepareDragAndDrop ():void {
    let idx:number
    let panelLst:HTMLCollection = document.getElementsByClassName('panel')

    for (idx = 0; idx < panelLst.length; idx += 1) {
        const panel = panelLst[idx]
        let lastPanel:HTMLElement

        panel.setAttribute('draggable', 'true')

        panel.addEventListener('dragstart', (e:any) => {
            e.dataTransfer.setData('text/plain', e.target.id)
        }, false)

        panel.addEventListener('dragover', (e:any) => {
           let target = e.target
           e.preventDefault();
            
            // Find correct container to stop painting childs.
            while (!target.classList.contains('panel')) {
                target = target.parentElement
            }
            
            lastPanel = target
            target.classList.add('panel--dragover')
        })

        panel.addEventListener('dragleave', (e:any) => {
            let target = e.target
            
            while (!(target.classList && target.classList.contains('panel'))) {
                target = target.parentElement
            }
            
            lastPanel.classList.remove('panel--dragover')
            e.preventDefault();
        })

        // The dragend event is fired when a drag operation is being ended (by releasing a mouse button or hitting the escape key).
        // panel.addEventListener('dragend', (e:any) => {
        //     let target = e.target

        //     while (!(target.classList && target.classList.contains('panel'))) {
        //         target = target.parentElement
        //     }
        //     lastPanel.classList.remove('panel--dragover')
        // })

        // NOTE CHROME: In order to have the drop event occur on a div element, 
        //    you must cancel the ondragenter and ondragover events.
        //    https://stackoverflow.com/questions/21339924/drop-event-not-firing-in-chrome#21341021
        panel.addEventListener('dragenter', (e:any) => {
            e.preventDefault();
        })

        panel.addEventListener('dragover', (e:any) => {
            e.preventDefault();
        })

        panel.addEventListener('drop', (e:any) => {
            let panelId = e.dataTransfer.getData('text/plain')

            e.preventDefault();
            // e.dataTransfer.clearData();  // psp,; FireFox NoModificationAllowedError: Modifications are not allowed for this document

            lastPanel.classList.remove('panel--dragover')
            panels.movePanelBeforePanel(panelId, e.currentTarget.id)
            panels.setFocusToPanel(panelId)
        }, false)
    }
}

/**
 * Set all EVENTS for this demo (buttons)
 * View port changes
 */
function setAllEvents ():void { 
    let btnAddNote:HTMLElement = document.getElementById('btnAddNote')!
    let btnAscending:HTMLElement = document.getElementById('btnAscending')!
    let btnClearView:HTMLElement = document.getElementById('btnClearView')!
    let btnMinimizeAll:HTMLElement = document.getElementById('btnMinimizeAll')!
    let btnlabHBA1cGlyHb:HTMLElement = document.getElementById('btnlabHBA1cGlyHb')!
    let btnOpenPresets:HTMLElement = document.getElementById('btnOpenPresets')!
    let btnPreset:HTMLElement = document.getElementById('btnPreset')!
    let btnPresetFilter:HTMLElement = document.getElementById('btnPresetFilter')!
    let btnSavePreset:HTMLElement = document.getElementById('btnSavePreset')!
    let btnSearch:HTMLElement = document.getElementById('btnSearch')!
    let btn30Days:HTMLElement = document.getElementById('btn30Days')!
    let btn100Days:HTMLElement = document.getElementById('btn100Days')!  
    let fldSearch:HTMLElement = document.getElementById('fldSearch')!
    let btnShareView:HTMLElement = document.getElementById('btnShareView')!
    let panelControlBar:HTMLElement = document.getElementById('panelControlBar')!

    let btnImplodeBiometrics:any
    let btnFooterContacts:any
    let timerId:any // To follow mouseMovement
    let mousePos = {x:0, y:0, y0:0, yr:0, wh: 0}

    // Window resize, with delay function
    // The timeout is killed and set again, when WINDOW is still dragged (resized)
    let delay = (() => {
        let timer = 0

        return function (callback:Function , ms:number) {
            clearTimeout (timer)
            timer = setTimeout(callback, ms)
        }
    })()

    // Minimaize ALL visible panels.
    btnMinimizeAll.addEventListener('click', ( () => { return panels.setStatusOfAllPanelsTo('MINIMAL') }))

    // A--Z Sorting on title
    btnAscending.addEventListener('click', () => panels.sortOnTitle())

    // Show PANELS, with DATA changes since date 30D / 100D ago.
    btn30Days.addEventListener('click', () => {
        panels.showPanelSet(presets.get(date30DaysPresetId).panelSet)
        panels.setFocusToPanel(presets.getPanelIds(date30DaysPresetId))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        stats()
    })
    btn100Days.addEventListener('click', () => {
        panels.showPanelSet(presets.get(date100DaysPresetId).panelSet)
        panels.setFocusToPanel(presets.getPanelIds(date100DaysPresetId))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        stats()
    })

    // //////////////////////////////////////////////////////////////
    // Functions to DRAG PANEL NAV BAR up and down.
    function getMousePosition() {
        let wrapper = document.getElementById('presetWrapperContainer')!

        if (mousePos.wh === 0) {
            mousePos.wh = wrapper.getBoundingClientRect().height!
        }

        wrapper.setAttribute('style', `height:${mousePos.wh + mousePos.yr}px;`)
        // console.log(mousePos, mousePos.wh)
    }

    function handleMouseMove(event:MouseEvent) {
        if (mousePos.y === 0) {
            mousePos.x = event.clientX,
            mousePos.y = event.clientY,
            mousePos.y0 = mousePos.y
            mousePos.yr = 0
            mousePos.wh = 0
        } else {
            mousePos.x = event.clientX,
            mousePos.y = event.clientY,
            mousePos.yr = mousePos.y - mousePos.y0
        }
    }

    function movePanelControlBar(e:Event) {
        let  panelControlBar = <HTMLElement>e.currentTarget
        // console.log(`Clicked on panel control bar`)
        panelControlBar.classList.remove('draggable')
        panelControlBar.classList.add('dragging')
        timerId = setInterval(getMousePosition, 100) // setInterval repeats every X ms
        // document.onmousemove = handleMouseMove;
        mousePos.y = 0 // Reset Y-ax movement calculation
        document.addEventListener('mousemove', handleMouseMove)
    }

    function panelControlBarMoved(e:Event) {
        let  panelControlBar = <HTMLElement>e.currentTarget
        // console.log(`stop moving panel control bar`)
        panelControlBar.classList.add('draggable')
        panelControlBar.classList.remove('dragging')
        clearInterval(timerId)
        document.removeEventListener('mousemove', handleMouseMove, false)
    }

    // Drag `panelControlBar` to size the window for presets.
    function setEventGrabPanelControlbar () {
        let  panelControlBar = document.getElementById('panelControlBar')!

        panelControlBar.classList.add('draggable')
        panelControlBar.addEventListener('mousedown', movePanelControlBar)
        panelControlBar.addEventListener('mouseup', panelControlBarMoved)
    }
    function revokeEventGrabPanelControlbar () {
        let  panelControlBar = document.getElementById('panelControlBar')!

        panelControlBar.classList.remove('draggable')
        panelControlBar.classList.remove('dragging')
        panelControlBar.removeEventListener("mousedown", movePanelControlBar, false);
        panelControlBar.removeEventListener("mouseup", panelControlBarMoved, false);
    }
    // END DRAG PANEL BAR functions
    // //////////////////////////////////////////////////////////////

    // Open / Close preset configuration panel
    // If preset panel is opened, then a new PANELCONTROL BAR behaviour will be active
    // The BAR can be moved UP and DOWN to increase the PRESET LIST WINDOW vertically.
    // Allowing the user to INCREASE the number of preset ROWS. Or decreasing them.
    btnPreset.addEventListener('click', () => document.getElementById('presetContainer')!.hidden = true)
    btnOpenPresets.addEventListener('click', () => {
        let  el = document.getElementById('presetContainer')!
        // flip flop
        el.hidden = el.hidden ? false : true

        // Fix USER FEEDBACK
        if (el.hidden) {
            revokeEventGrabPanelControlbar()
        } else {
            setEventGrabPanelControlbar()
        }
    })

    // PRESET Filter
    btnPresetFilter.addEventListener('click', (e) => {
        if ((<HTMLElement>e.target!).innerHTML === 'Alles' ) {
            (<HTMLElement>e.target!).innerHTML = 'Persoonlijk'
            presets.showSystemPresets(true)
        } else {
            (<HTMLElement>e.target!).innerHTML = 'Alles'
            presets.showSystemPresets(false)
        }
    })

    // Preset SaveAs 
    btnSavePreset.addEventListener('click', (e:MouseEvent) => {
        const stateSet = panels.getCurrentState()
        
        let btnSavePresetAs:HTMLElement
        let btnCancelPreset:HTMLElement

        buildSaveAsDialog(stateSet)

        // Dialog "SaveAs" buttons
        btnSavePresetAs = document.getElementById('btnSavePresetAs')!
        btnCancelPreset = document.getElementById('btnCancelPreset')!

        // Cancel dialog, don't do anything
        btnCancelPreset.addEventListener('click', function eventBtnCancel () {
            let overlay = document.getElementById('overlayModelessDialog')!

            overlay.parentNode!.removeChild(overlay)
            // TODO: Remove 'SaveAs'-button also ...
            btnCancelPreset.removeEventListener("click", eventBtnCancel, false);
        })

        // Save PRESET as ...
        btnSavePresetAs.addEventListener('click', function eventBtnSaveAs () {
            let overlay = document.getElementById('overlayModelessDialog')!

            // Prepare default PRESET Callback function
            fixTS = {
                label: (<HTMLInputElement>document.getElementsByName('presetName')[0]!).value,
                description: (<HTMLInputElement>document.getElementsByName('presetDescription')[0]!).value,
                role: 'USER',
                stateSet: <StateSet[]>stateSet,
                callBack: () => {}
            }
            fixTS.callBack = ((stateSet:StateSet[]) => {
                return (state:State) => {
                    let panelLst:string[] = []
            
                    if (state) {
                        stateSet.forEach( (stateSet:StateSet) => {
                            stateSet.state = state
                            panelLst.push(stateSet.id!)
                        })
            
                        if (state === 'CLOSE') {
                            panels.deactivatePanels(panelLst)
                            stats()
                            return
                        }
                    }
                    panels.showPanelSet(stateSet)
                    panels.setFocusToPanel(presets.getPanelIds(stateSet))
                    notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                    updatePresetButtonLabel()
                    stats()
                }
            })(fixTS.stateSet)
            
            presets.add(fixTS)
            presets.buildPresetList()
            // Hide system presets, show users preferals by default
            document.getElementById('btnPresetFilter')!.innerHTML = 'Alles'
            presets.showSystemPresets(false)

            // Change preset button label to current SAVED preset label.
            updatePresetButtonLabel()

            // Cleanup overlay modeless dialog window
            // TODO: Remove 'Close'-button also ...
            overlay.parentNode!.removeChild(overlay)
            btnSavePresetAs.removeEventListener("click", eventBtnSaveAs, false);
        })
    })

    // Copy share LINK to dashboard preset view.
    btnShareView.addEventListener('click', (e:MouseEvent) => {
        let btnCopyUri:HTMLElement
        let btnCancelUri:HTMLElement

        buildCopyLinkDialog()

        // Dialog "CopyURI" buttons
        btnCopyUri = document.getElementById('btnCopyUri')!
        btnCancelUri = document.getElementById('btnCancelUri')!
        
        // Cancel dialog, don't do anything
        btnCancelUri.addEventListener('click', function eventBtnCancelUri () {
            let overlay = document.getElementById('overlayModelessDialog')!

            overlay.parentNode!.removeChild(overlay)
            // TODO: Remove 'SaveAs'-button also ...
            btnCancelUri.removeEventListener("click", eventBtnCancelUri, false);
            overlay = document.getElementById('overlayModelessDialog')!
            overlay.parentNode!.removeChild(overlay)
        })

        // Cancel dialog, don't do anything
        btnCopyUri.addEventListener('click', function eventBtnCopyUri () {
            let overlay = document.getElementById('overlayModelessDialog')!

            overlay.parentNode!.removeChild(overlay)
            // TODO: Remove 'SaveAs'-button also ...
            btnCopyUri.removeEventListener("click", eventBtnCopyUri, false);
            overlay = document.getElementById('overlayModelessDialog')!
            overlay.parentNode!.removeChild(overlay)
        })

    })

    // Show "Add a Note" panel
    btnAddNote.addEventListener('click', (e:MouseEvent) => {
        // Current DASHBOARD panel view, where the notes are related to
        let btnNoteNav:HTMLElement
        let notePanelId = 'system-addNotes'

        // First check if 'Add note' panel isn't active already?
        if (!panels.findPanelByID(notePanelId).active) {
            // Button container, not the click on the button itself
            // Buttons in container 'ADD NOTE', 'CANCEL'.
            btnNoteNav = document.getElementById('btnNoteNav')!
    
            panels.activatePanels([notePanelId])
            panels.setFocusToPanel(notePanelId)
            stats()

            // Handle button click on PANEL HTML ELEMENTS.
            btnNoteNav.addEventListener('click', function eventPanelEnterNote (e:Event) {
                let current_datetime = new Date()
                let formNode = document.getElementById('formNode')!
                // Build a new preset
                let presetID:string 
                let stateSet:StateSet[]
                let target = e.target!

                stateSet = panels.getCurrentState()

                e.stopPropagation()

                // Prepare default PRESET Callback function
                fixTS = {
                    label: 'Dashboard',
                    description: 'Dashboard view while adding a note on ' +  current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds(),
                    role: 'NOTE',
                    stateSet: <StateSet[]>stateSet,
                    callBack: () => {}
                }
                fixTS.callBack = ((stateSet:StateSet[]) => {
                    return (state:State) => {
                        let panelLst:string[] = []
                
                        if (state) {
                            stateSet.forEach( (stateSet:StateSet) => {
                                stateSet.state = state
                                panelLst.push(stateSet.id!)
                            })
                
                            if (state === 'CLOSE') {
                                panels.deactivatePanels(panelLst)
                                stats()
                                return
                            }
                        }
                        panels.showPanelSet(stateSet)
                        panels.setFocusToPanel(presets.getPanelIds(stateSet))
                        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                        updatePresetButtonLabel()
                        stats()
                    }
                })(fixTS.stateSet)
                
                if ((<HTMLElement>target).nodeName.toUpperCase() === 'BUTTON') {
                    // 
                    switch ((<HTMLElement>target).innerText) {
                        case 'Toevoegen':
                            presetID = presets.add(fixTS)
                            presets.buildPresetList()
                
                            notes.add({
                                noteAuthor: 'me',
                                noteCreated: current_datetime,
                                noteText: (<any>formNode.children[0]).value,
                                preset: presets.get(presetID)
                            })

                            break
                        default:
                        console.log(`Panel 'Add note' button function '${(<HTMLElement>target).innerText}' missing.`)
                    }

                    btnNoteNav.removeEventListener('click', eventPanelEnterNote, false)
                    panels.deactivatePanels(['system-addNotes'])
                }
            })
        }
    })

    // On view port dimension change
    window.onresize = (() => {
        delay(function(){
            // Redraw panels to correct VIEWPORT dimension, in just a half second
            let viewPortWidth = window.innerWidth
            if (viewPortWidth < 960) {
                // Change the grid
                panels.resizeGrid({columns:8, rows:gridRow * 4})
            } else {
                panels.resizeGrid({columns:12, rows:gridRow * 4})
            }
        }, 500)
    })

    // Clear the current view
    btnClearView.addEventListener('click', () => {
        panels.deactivatePanels(panels.findPanels({isActive: true})) // Currently visible on screen
        // Algoritm states, that even the notes pannel can have notes - no other presets (panels) shown.
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        document.getElementById('btnOpenPresets')!.innerHTML = 'Preset \'EMPTY\''
        stats()
    })

    // Search button
    btnSearch.addEventListener('click', () => {
        panels.searchFor((<HTMLInputElement>fldSearch).value)
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    })

    // Anble to start search with ENTER-key
    fldSearch.onkeydown = function(e:any){
        if (e.keyCode === 13) {
            panels.searchFor((<HTMLInputElement>fldSearch).value)
            notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
            updatePresetButtonLabel()
            stats()
        }
    }

    // Implode BIOMETRIC PANELS into one single panel.
    btnImplodeBiometrics = [].slice.call(document.getElementsByClassName('btn--implode'))
    for (let node of btnImplodeBiometrics) {
        node.addEventListener('click', ((n) => {
            return (e:Event) => {
                // Activate IMPLODED panel BIOMETRICS
                // De-activate EXPLODED BIOMETRICS panels
                panels.activatePanels(['imploded-biometrics'])
                panels.deactivatePanels([
                    'lsp-oxygenSaturation',
                    'lsp-height',
                    'lsp-bodyTemperature',
                    'lsp-bloodpressure',
                    'lsp-clinicalChemistry',
                    'lsp-bodyWeight',
                    'lsp-respirationFrequency',
                    'lsp-pulseFrequency',
                    'podo-bmi',
                    'podo-tailleOmtrek',
                ])
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                stats()
            }
        })(node))
    }

    // Explode a panel into individual panels.
    btnImplodeBiometrics = [].slice.call(document.getElementsByClassName('btn--explode'))
    for (let node of btnImplodeBiometrics) {
        node.addEventListener('click', ((n) => {
            return (e:Event) => {
                // Activate IMPLODED panel BIOMETRICS
                // De-activate EXPLODED BIOMETRICS panels
                panels.activatePanels([
                    'lsp-oxygenSaturation',
                    'lsp-height',
                    'lsp-bodyTemperature',
                    'lsp-bloodpressure',
                    'lsp-clinicalChemistry',
                    'lsp-bodyWeight',
                    'lsp-respirationFrequency',
                    'lsp-pulseFrequency',
                    'podo-bmi',
                    'podo-tailleOmtrek',
                ])
                panels.deactivatePanels(['imploded-biometrics'])
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                stats()
            }
        })(node))
    }

    // open related CONTACT panels
    btnFooterContacts = [].slice.call(document.getElementsByClassName('btn--contacts'))
    for (let node of btnFooterContacts) {
        node.addEventListener('click', ((n) => {
            return (e:Event) => {
                panels.showPanelSet(presets.get(contactPresetId).panelSet)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                stats()
            }
        })(node))
    }

    // Explode a LAB Value HBA1c / GlyHb
    btnlabHBA1cGlyHb.addEventListener('click', () => {
        panels.activatePanels([
            'lsp-labHBA1cGlyHb',
        ])
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        stats()
    })

    prepareDragAndDrop()
}

/**
 * Show panel statistics on screen.
 * More a debug function
 */
function stats ():void {
    let statsPanels:HTMLElement = document.getElementById('statsPanels')!

    statsPanels.innerHTML = panels.countPanels().active.toString()
}

/**
 * Depending on the user actions, the current panel view can be 'UNSAVED AS A PRESET'.
 */
function updatePresetButtonLabel ():void {
    // Find preset MATCHING current presets (panels) view
    let preset = presets.checkIfPresetExists({panelIDs:panels.findPanels({isActive: true})})

    if (preset) {
        document.getElementById('btnOpenPresets')!.innerHTML = `Preset '${preset.label}'`
    } else {
        document.getElementById('btnOpenPresets')!.innerHTML = 'Preset \'CHANGED\''
    }
}


//////////////////////////////////////////////////////////////////////////
// PREPARE DEMO //////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// Add all needed DEMONSTRATION Event listerers 
// Panel menu first
addPanelMenuEventListeners()
// As last other events, because these depends on `addmenu()`
setAllEvents()

// Build PRESETS /////////////////////////////////////////////////////////
// Preset ROLES (who owns them)
// - USER, is a user declared PRESET
// - SYSTEM, is a SYSTEM preset, can not be modified by user
// - STICKY, is a SYSTEM preset, can not be modified by user AND alsways visible 

// BIOMETRICS

let biometrics:StateSet[] = []

biometrics.push({id: 'lsp-oxygenSaturation', title: 'Oxygen saturation', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
biometrics.push({id: 'lsp-height', title: 'Height', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
biometrics.push({id: 'lsp-bodyTemperature', title: 'Temperature body', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
biometrics.push({id: 'lsp-bloodpressure', title: 'Bloodpresure', hasData: true, semaphore: 'MEDIUM', semaphoreType: 'DOWN', state: 'MINIMAL', seq: 3})
biometrics.push({id: 'lsp-clinicalChemistry', title: 'Clinical chemistry', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})
biometrics.push({id: 'lsp-bodyWeight', title: 'Weight body', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 5})
biometrics.push({id: 'lsp-respirationFrequency', title: 'Respiration frequency', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 6})
biometrics.push({id: 'lsp-pulseFrequency', title: 'Pulse frequency', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 7})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTS = {
    label: 'Biometrics',
    description: 'Show patient biometrics',
    role: 'SYSTEM',
    stateSet: <StateSet[]>biometrics,
    callBack: () => {}
}
// Write a function with PANEL module reference to be executed in the preset module.
// Part of the preset EVENT handler, when selecting a preset or preset PANEL state behaviour.
fixTS.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        // Keep a list of involved panels, to be used when preset event `close-panels` occurs.
        let panelCloseLst:string[] = []

        // A panel STATE can be empty, meaning use panel states stores as preset
        // If SET then all panel PRESET states will be overruled by this STATE.
        // Meaning all preset panels will have the same state.
        if (state) {
            // Overrule PRESET panel states
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelCloseLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelCloseLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTS.stateSet)

biometricsPresetId = presets.add(fixTS)

// CONTACTS

let contacts:StateSet[] = []

contacts.push({id: 'dashContacts', title: 'Contacts', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
contacts.push({id: 'lsp-contactMoments', title: 'Contact moments', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
contacts.push({id: 'lsp-contactReports', title: 'Contact reports', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
contacts.push({id: 'lsp-contactHistory', title: 'Contact history', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTS = {
    label: 'Contacts',
    description: 'Show patient contacts and reports',
    role: 'SYSTEM',
    stateSet: <StateSet[]>contacts,
    callBack: () => {}
}
fixTS.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTS.stateSet)

contactPresetId = presets.add(fixTS)

// 30 Days CHANGE

let days30:StateSet[] = []

days30.push({id: 'lsp-oxygenSaturation', title: 'Oxygen saturation', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
days30.push({id: 'lsp-bodyTemperature', title: 'Temperature body', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
days30.push({id: 'lsp-alerts', title: 'Alerts', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTS = {
    label: 'Last 30 days',
    description: 'Show changes in the last 30 days',
    role: 'SYSTEM',
    stateSet: <StateSet[]>days30,
    callBack: () => {}
}
fixTS.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTS.stateSet)

date30DaysPresetId = presets.add(fixTS)

// 100 Days CHANGE

let days100:StateSet[] = []

days100.push({id: 'lsp-oxygenSaturation', title: 'Oxygen saturation', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
days100.push({id: 'lsp-bodyTemperature', title: 'Temperature body', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
days100.push({id: 'lsp-alerts', title: 'Alerts', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
days100.push({id: 'lsp-clinicalChemistry', title: 'Clinical chemistry', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})
days100.push({id: 'lsp-contactMoments', title: 'Contact moments', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})
days100.push({id: 'lsp-contactReports', title: 'Contact reports', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 5})
​
// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTS = {
    label: 'Last 100 days',
    description: 'Show changes in the last 100 days',
    role: 'SYSTEM',
    stateSet: <StateSet[]>days100,
    callBack: () => {}
}
fixTS.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTS.stateSet)

// Remember preset ID to use on button filter functions
date100DaysPresetId = presets.add(fixTS)

// ADD ALL INDIVIDUAL (system) PANELS as PRESETS

panels.findPanels().forEach((id:string) => {
    let single:StateSet[] = []
    let panel = panels.findPanelByID(id)

    single.push({id: panel.id, title: panel.title, hasData: panel.hasData, semaphore: panel.semaphore, semaphoreType: panel.semaphoreType, state: 'MINIMAL', seq: 0})

    // A TypeScript fix, to pass typescript `callback` problems
    // Because I wanted to try Object reference, while building the callback function.
    fixTS = {
        label: `${panel.title} (system)`,
        description: `${panel.title}, a system panel`,
        role: 'SYSTEM',
        stateSet: single,
        callBack: () => {}
    }
    fixTS.callBack = ((stateSet:StateSet[]) => {
        return (state:State) => {
            let panelLst:string[] = []
    
            if (state) {
                stateSet.forEach( (stateSet:StateSet) => {
                    stateSet.state = state
                    panelLst.push(stateSet.id!)
                })
    
                if (state === 'CLOSE') {
                    panels.deactivatePanels(panelLst)
                    notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true, exclude:['appl-notes', 'system-addPanel']})}))
                    updatePresetButtonLabel()
                    return
                }
            }
            panels.showPanelSet(stateSet)
            panels.setFocusToPanel(presets.getPanelIds(stateSet))
            notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true, exclude:['appl-notes', 'system-addPanel']})}))
            console.log(fixTS.label, panels.findPanels({isActive: true, exclude:['appl-notes', 'system-addPanel']}))
            updatePresetButtonLabel()
            stats()
        }
    })(fixTS.stateSet)

    presets.add(fixTS)
})

// DIABETES - A.) A specific layout for dieticians (NEED INPUT OF USERS)

let diabetes:StateSet[] = []

diabetes.push({id: 'appl-notes', title: 'Notes', hasData: false, semaphore: 'HIGH', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
// diabetes.push({id: 'dashMedication', title: 'Medication', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
diabetes.push({id: 'dashMedication', title: 'Medication', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'EXPANDED', seq: 1})
// diabetes.push({id: 'podo-behandelhistorie', title: 'Behandelhistorie', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
diabetes.push({id: 'podo-behandelhistorie', title: 'Behandelhistorie', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'EXPANDED', seq: 2})
// diabetes.push({id: 'podo-automatischBehandeladvies', title: 'Automatisch behandeladvies', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})
diabetes.push({id: 'podo-automatischBehandeladvies', title: 'Automatisch behandeladvies', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'SUMMARY', seq: 3})

// diabetes.push({id: 'podo-monitorDiagnoseRisicofactoren', title: 'Diagnose en Risicofactoren', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})
diabetes.push({id: 'podo-monitorDiagnoseRisicofactoren', title: 'Diagnose en Risicofactoren', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'SUMMARY', seq: 4})
// diabetes.push({id: 'podo-uitkomstenProces', title: 'Uitkomsten en Proces', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 5})
diabetes.push({id: 'podo-uitkomstenProces', title: 'Uitkomsten en Proces', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'SUMMARY', seq: 5})
// diabetes.push({id: 'podo-memo', title: 'Memo', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 6})
diabetes.push({id: 'podo-memo', title: 'Memo', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'SUMMARY', seq: 6})
// diabetes.push({id: 'podo-behandelteam', title: 'Behandelteam', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 7})
diabetes.push({id: 'podo-behandelteam', title: 'Behandelteam', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'EXPANDED', seq: 7})

diabetes.push({id: 'lsp-labHBA1cGlyHb', title: 'Lab HbA1c / GlyHb', hasData: true, semaphore: 'LOW', semaphoreType: 'UP', state: 'MINIMAL', seq: 8}) // Show graph
diabetes.push({id: 'lsp-labDeterminations', title: 'Lab determinations', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 9})
diabetes.push({id: 'podo-footControlLast', title: 'Voetcontrole (Diabetes)', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 10})
diabetes.push({id: 'podo-problemenKlachten', title: 'Problemen/Klachten', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 11})
diabetes.push({id: 'podo-leefstijl', title: 'Leefstijl', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 12})
diabetes.push({id: 'lsp-bloodpressure', title: 'Bloeddruk', hasData: true, semaphore: 'MEDIUM', semaphoreType: 'DOWN', state: 'MINIMAL', seq: 13})
diabetes.push({id: 'lsp-bodyWeight', title: 'Lichaamsgewicht', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 14})
diabetes.push({id: 'podo-bmi', title: 'BMI', hasData: true, semaphore: 'LOW', semaphoreType: 'DOWN', state: 'MINIMAL', seq: 15})
diabetes.push({id: 'podo-fundusControlLast', title: 'Fundus', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 16})
diabetes.push({id: 'podo-24hbloeddruk', title: '24 uurs bloeddruk', hasData: false, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 17})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTSDiabetes = {
    label: 'Diëtist',
    description: 'Diabetes regular',
    role: 'DIETICIAN',
    stateSet: <StateSet[]>diabetes,
    callBack: () => {}
}
fixTSDiabetes.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTSDiabetes.stateSet)

presets.add(fixTSDiabetes)


// PODOTHERAPIST - B.) A specific layout for podotherapist (ONE USER INPUT)

let podotherapist:StateSet[] = []

podotherapist.push({id: 'lsp-izp', title: 'Individual Care Plan', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
podotherapist.push({id: 'podo-monitorDiagnoseRisicofactoren', title: 'Diagnose en Risicofactoren', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
podotherapist.push({id: 'podo-uitkomstenProces', title: 'Uitkomsten en Proces', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
podotherapist.push({id: 'podo-memo', title: 'Memo', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})
podotherapist.push({id: 'podo-automatischBehandeladvies', title: 'Automatisch behandeladvies', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})
podotherapist.push({id: 'podo-behandelhistorie', title: 'Behandelhistorie', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 5})
podotherapist.push({id: 'dashMedication', title: 'Medication', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 6})
podotherapist.push({id: 'podo-behandelteam', title: 'Behandelteam', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 7})
podotherapist.push({id: 'lsp-labDeterminations', title: 'Lab determinations', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 8})
podotherapist.push({id: 'podo-footControlLast', title: 'Voetcontrole (Diabetes)', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 9})
podotherapist.push({id: 'podo-problemenKlachten', title: 'Problemen/Klachten', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 10})
podotherapist.push({id: 'podo-leefstijl', title: 'Leefstijl', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 11})
podotherapist.push({id: 'lsp-bloodpressure', title: 'Bloeddruk', hasData: true, semaphore: 'MEDIUM', semaphoreType: 'DOWN', state: 'MINIMAL', seq: 12})
podotherapist.push({id: 'lsp-bodyWeight', title: 'Lichaamsgewicht', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 13})
podotherapist.push({id: 'podo-fundusControlLast', title: 'Fundus', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 14})
podotherapist.push({id: 'podo-bmi', title: 'BMI', hasData: true, semaphore: 'LOW', semaphoreType: 'DOWN', state: 'MINIMAL', seq: 15})
podotherapist.push({id: 'podo-24hbloeddruk', title: '24 uurs bloeddruk', hasData: false, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 16})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTSPodotherapist = {
    label: 'Podotherapist',
    description: 'Podotherapist regular',
    role: 'PODOTHERAPIST',
    stateSet: <StateSet[]>podotherapist,
    callBack: () => {}
}
fixTSPodotherapist.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTSPodotherapist.stateSet)

presets.add(fixTSPodotherapist)

// POH - C.) Praktijkondersteuning huisarts
//     PGO's generiek (zou de eerste gebruikers groep kunnen zijn)

// OK - EPISODE: Historisch overzicht van behandelingen (start/stop copd, diabetisch, trombose, gebroken been, etc... Volgens PSP zijn dat EVENTS. (viseel, barchart).
// OK - CCQ onderzoek, ingevoerd door de patient (eigen observatie). Feedback geven...
// OK - IZP Zorgplan

// POH Bezoek (wo. 5 feb 2020, Annelies Lodewijk, praktijkondersteuner Huisartsenpraktijk P.E.G. Gielen)
// Hieruit bleek dat zij geen interesse hebben in de PV Monitor schermen, die patient info zit al in het HIS ...
// Wel veel wensen met betrekking tot andere informatie bronnen rond de patient zorg, zoals; 
// - thuiszorg
// - mantelzorg
// - sociale kaart
// - Panel met handige hyperlinks

// Eerder was al bekend dat;
// - Ouderenzorg
// - Thuiszorg
// - Wijkverpleging
// - Geriatrie, kwetsbare ouderen
// - MDO
// - SMFPC, Sociaal, Communicatief, ...
// - Behandel ristricties
// - " Last will "
// - Ontslagbrieven ziekenhuis, etc.
// - FitBit

// NOTES 200211 - Ik maak een extra POH preset aan, met informatie panels derden (thuiszorg, ...)

let POH:StateSet[] = []

POH.push({id: 'appl-notes', title: 'Notes', hasData: false, semaphore: 'HIGH', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
POH.push({id: 'podo-monitorDiagnoseRisicofactoren', title: 'Diagnose en Risicofactoren', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
POH.push({id: 'podo-uitkomstenProces', title: 'Uitkomsten en Proces', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
POH.push({id: 'podo-memo', title: 'Memo', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})
POH.push({id: 'podo-automatischBehandeladvies', title: 'Automatisch behandeladvies', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})
POH.push({id: 'podo-behandelhistorie', title: 'Behandelhistorie', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 5})
POH.push({id: 'dashMedication', title: 'Medication', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 6})
POH.push({id: 'podo-behandelteam', title: 'Behandelteam', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 7})
POH.push({id: 'dashEpisodes', title: 'Individual Care Plan', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 8})
POH.push({id: 'fysio-CCQ', title: 'CCQ', hasData: false, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 9})
POH.push({id: 'lsp-izp', title: 'Individual Care Plan', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 10})
POH.push({id: 'poh-ontslagbrieven', title: 'Ontslagbrieven', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 11})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTSPOH = {
    label: 'POH',
    description: 'Monitor Portvita',
    role: 'POH',
    stateSet: <StateSet[]>POH,
    callBack: () => {}
}
fixTSPOH.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTSPOH.stateSet)

presets.add(fixTSPOH)

// 200211 - POH, dedicated dataset, with thuiszorg etc.

let POHSecondLine:StateSet[] = []

POHSecondLine.push({id: 'fysio-CCQ', title: 'CCQ', hasData: false, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
POHSecondLine.push({id: 'lsp-izp', title: 'Individual Care Plan', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
POHSecondLine.push({id: 'poh-two-zorgplan-reeshof', title: 'Zorgplan - Reeshof', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
POHSecondLine.push({id: 'poh-ontslagbrieven', title: 'Ontslagbrieven', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})
POHSecondLine.push({id: 'poh-two-weblinks', title: 'Nuttige links', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})
POHSecondLine.push({id: 'poh-two-sociaaldomein', title: 'Sociaal domein', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 5})
POHSecondLine.push({id: 'poh-two-thuiszorg', title: 'Thuiszorg', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 6})
POHSecondLine.push({id: 'poh-two-mantelzorg', title: 'Mantelzorg', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 7})
POHSecondLine.push({id: 'poh-two-ouderenzorg', title: 'Ouderenzorg', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 8})
POHSecondLine.push({id: 'poh-two-wijkverpleging', title: 'Wijkverpleging', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 9})
POHSecondLine.push({id: 'poh-two-geriatrie', title: 'Geriatrie', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 10})
POHSecondLine.push({id: 'poh-two-mdo', title: 'MDO', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 11})
POHSecondLine.push({id: 'poh-two-behandel-ristricties', title: 'Behandel-ristricties', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 12})
POHSecondLine.push({id: 'poh-two-last-will', title: 'Last will', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 13})
POHSecondLine.push({id: 'poh-two-fitbit', title: 'FitBit', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 14})
POHSecondLine.push({id: 'system-sources', title: 'Information providers', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 15})
POHSecondLine.push({id: 'poh-two-digid', title: 'DIGID', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 16})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTSPOHSecondLine = {
    label: 'POH Externe zorg',
    description: 'Informatie panelen betreffende zorgproviders patiënt',
    role: 'POH',
    stateSet: <StateSet[]>POHSecondLine,
    callBack: () => {}
}
fixTSPOHSecondLine.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTSPOHSecondLine.stateSet)

presets.add(fixTSPOHSecondLine)



// PODOTHERAPIST - D.) A specific layout for physiotherapist (NONE USER INPUT, using PV monitor)
//                     See -Huisartsen Eemland Zorg - Fysiotherapie Fyzie - EEMLA1, PVT
let physiotherapist:StateSet[] = []

physiotherapist.push({id: 'lsp-izp', title: 'Individual Care Plan', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
physiotherapist.push({id: 'podo-monitorDiagnoseRisicofactoren', title: 'Diagnose en Risicofactoren', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
physiotherapist.push({id: 'podo-uitkomstenProces', title: 'Uitkomsten en Proces', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
physiotherapist.push({id: 'podo-memo', title: 'Memo', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})
physiotherapist.push({id: 'podo-automatischBehandeladvies', title: 'Automatisch behandeladvies', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})
physiotherapist.push({id: 'podo-behandelhistorie', title: 'Behandelhistorie', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 5})
physiotherapist.push({id: 'dashMedication', title: 'Medication', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 6})
physiotherapist.push({id: 'podo-behandelteam', title: 'Behandelteam', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 7})
physiotherapist.push({id: 'fysio-Spirometry', title: 'Spirometry', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 8})
physiotherapist.push({id: 'lsp-labDeterminations', title: 'Lab determinations', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 9})
physiotherapist.push({id: 'fysio-diseaseBurdenMeter', title: 'Disease burden meter (COPD)', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 10})
physiotherapist.push({id: 'podo-leefstijl', title: 'Leefstijl', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 11})
physiotherapist.push({id: 'lsp-bodyWeight', title: 'Lichaamsgewicht', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 12})
physiotherapist.push({id: 'podo-bmi', title: 'BMI', hasData: true, semaphore: 'LOW', semaphoreType: 'DOWN', state: 'MINIMAL', seq: 13})
physiotherapist.push({id: 'lsp-respirationFrequency', title: 'Respiration frequency', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 14})
physiotherapist.push({id: 'lsp-auscultation', title: 'Auscultation', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 15})
physiotherapist.push({id: 'fysio-tonvormigeBorstkas', title: 'Tonvormige borstkas', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 16})
physiotherapist.push({id: 'fysio-useAuxiliaryRespiratoryMuscles', title: 'Using auxiliary respiratory muscles', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 17})
physiotherapist.push({id: 'fysio-stoppenMetRoken', title: 'Stoppen met roken', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 18})
physiotherapist.push({id: 'fysio-exacerbatie', title: 'Exacerbatie (Astma/COPD)', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 19})
physiotherapist.push({id: 'fysio-CCQ', title: 'CCQ', hasData: false, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 20})
physiotherapist.push({id: 'fysio-MRCDyspnoeschaal', title: 'MRC-dyspnoeschaal', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 21})
physiotherapist.push({id: 'fysio-klinimetrieFysiotherapie', title: 'Klinimetrie Fysiotherapie ', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 22})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTSPhysiotherapist = {
    label: 'Physiotherapist',
    description: 'Physiotherapist regular',
    role: 'PHYSIOTHERAPIST',
    stateSet: <StateSet[]>physiotherapist,
    callBack: () => {}
}
fixTSPhysiotherapist.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTSPhysiotherapist.stateSet)

presets.add(fixTSPhysiotherapist)

// MONITOR - E.) BEHANDELING

let monitorBehandeling:StateSet[] = []

monitorBehandeling.push({id: 'podo-monitorDiagnoseRisicofactoren', title: 'Diagnose en Risicofactoren', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 0})
monitorBehandeling.push({id: 'podo-uitkomstenProces', title: 'Uitkomsten en Proces', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
monitorBehandeling.push({id: 'podo-memo', title: 'Memo', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 2})
monitorBehandeling.push({id: 'podo-automatischBehandeladvies', title: 'Automatisch behandeladvies', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})
monitorBehandeling.push({id: 'podo-behandelhistorie', title: 'Behandelhistorie', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})
monitorBehandeling.push({id: 'dashMedication', title: 'Medication', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 5})
monitorBehandeling.push({id: 'podo-behandelteam', title: 'Behandelteam', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 6})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTS = {
    label: 'Monitor behandeling',
    description: 'Behandeling gerelateerde informatie',
    role: 'SYSTEM',
    stateSet: <StateSet[]>monitorBehandeling,
    callBack: () => {}
}
fixTS.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTS.stateSet)

presets.add(fixTS)


// MONITOR - A.) BEHANDELING

let monitorLichamelijkOnderzoek:StateSet[] = []

monitorLichamelijkOnderzoek.push({id: 'lsp-bloodpressure', title: 'Bloeddruk', hasData: true, semaphore: 'MEDIUM', semaphoreType: 'DOWN', state: 'MINIMAL', seq: 0})
monitorLichamelijkOnderzoek.push({id: 'lsp-bodyWeight', title: 'Lichaamsgewicht', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 1})
monitorLichamelijkOnderzoek.push({id: 'podo-bmi', title: 'BMI', hasData: true, semaphore: 'LOW', semaphoreType: 'DOWN', state: 'MINIMAL', seq: 2})
monitorLichamelijkOnderzoek.push({id: 'podo-tailleOmtrek', title: 'Tailleomtrek', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 3})
monitorLichamelijkOnderzoek.push({id: 'podo-bloedglucoseNuchter', title: 'Bloedglucose Nuchter', hasData: true, semaphore: 'NONE', semaphoreType: 'NONE', state: 'MINIMAL', seq: 4})

// A TypeScript fix, to pass typescript `callback` problems
// Because I wanted to try Object reference, while building the callback function.
fixTS = {
    label: 'Lichamelijk onderzoek',
    description: 'Lichamelijk onderzoek',
    role: 'SYSTEM',
    stateSet: <StateSet[]>monitorLichamelijkOnderzoek,
    callBack: () => {}
}
fixTS.callBack = ((stateSet:StateSet[]) => {
    return (state:State) => {
        let panelLst:string[] = []

        if (state) {
            stateSet.forEach( (stateSet:StateSet) => {
                stateSet.state = state
                panelLst.push(stateSet.id!)
            })

            if (state === 'CLOSE') {
                panels.deactivatePanels(panelLst)
                notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
                updatePresetButtonLabel()
                stats()
                return
            }
        }
        panels.showPanelSet(stateSet)
        panels.setFocusToPanel(presets.getPanelIds(stateSet))
        notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))
        updatePresetButtonLabel()
        stats()
    }
})(fixTS.stateSet)

presets.add(fixTS)

// Get DEMO preparations, using URI GET properties
// http://127.0.0.1:8080/?role=PODOTHERAPIST
const uriProps = getURLProperties()
let demoRole = 'PODOTHERAPIST'
if (uriProps.hasOwnProperty('role')) {
    demoRole = getURLProperties().role
}

// Hide presets UI by default
document.getElementById('presetContainer')!.hidden = true
// Set default demo role
presets.setRole(demoRole)
// Show at first only personal / ROLE specific presets
presets.buildPresetList()
// Hide system presets, show users preferals by default
// AND the active ROLE of the user 'Podotherapist', etc.
presets.showSystemPresets(false)

// Fill demo data with 'Notes' on all presets.
// Find presets with no parameters. Results in finding all.
notes.__demoFillDatabaseNotes(presets.findPresets())
// Activate Note panel feedback
// notes.init() // DISABLED - Because EJ does not want messaging (berichten).

// Force 'Add panel'-panel, visible and sticky
panels.setPanelToSticky ('system-addPanel', 'BOTTOM')

// Hide all panels
panels.deactivatePanels(panels.findPanels({isActive: true})) // Currently visisble on screen

// Show DATA as a role of ...
switch (demoRole.toUpperCase()) {
    case 'DIETICIAN':
        panels.showPanelSet(fixTSDiabetes.stateSet)
        document.getElementById('btnOpenPresets')!.innerHTML = 'Preset \'Diëtist\''
        break
    case 'PHYSIOTHERAPIST':
        panels.showPanelSet(fixTSPhysiotherapist.stateSet)
        document.getElementById('btnOpenPresets')!.innerHTML = 'Preset \'Physiotherapeut\''
        break
    case 'PODOTHERAPIST':
        panels.showPanelSet(fixTSPodotherapist.stateSet)
        document.getElementById('btnOpenPresets')!.innerHTML = 'Preset \'Podotherapeut\''
        break
    case 'POH':
        // panels.showPanelSet(fixTSPOH.stateSet)
        panels.showPanelSet(fixTSPOHSecondLine.stateSet)
        document.getElementById('btnOpenPresets')!.innerHTML = 'Preset \'POH Externe zorg\''
        break
    case 'BLANK':
        document.getElementById('btnOpenPresets')!.innerHTML = 'Preset \'EMPTY\''
        break
    default:
        console.error(`Unknown ROLE "${demoRole}" defined in URI.`)
}

// Get specific notes, for given presets, based on current visible panels.
// It depends on the visibility (availabillity) of of the notes panel.
notes.updateNotesView(presets.findPresets({panelIDs:panels.findPanels({isActive: true})}))

// Show some STATISTICS of the panels, on the DASHBOARD.
stats()
