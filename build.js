/**
 * With this build file the MOCK-UP application is created.
 * And generated inside the `./build/` folder.
 * Start a browser and drag `./build/index.html` into the browser to start the mock-up.
 *
 * Your can copy the content of `./build` to the appropriate location on that server.
 *
 * USAGE: execute `npm start` on the NodeJS console.
 * The site is generated inside the build folder.
 */

// NodeJS libraries

const fse = require('fs-extra')
const path = require('path')
const rimraf = require('rimraf')
const sass = require('node-sass')

// Configuration

let sassConfig = {
  files: ['main.scss', 'normalize.scss'],
  bld: 'build/css/',
  src: '/src/assets/css/'
}
let copyAssetsConfig = {
  maps: ['img', 'js'],
  bld: 'build/',
  src: '/src/assets/'
}
let copyHtmlConfig = {
  maps: [''], // Means, complete `src` folder
  bld: 'build/',
  src: '/src/html/'
}

/**
 * Delete the contents of the `./build` directory
 */

function cleanBuild () {
  rimraf.sync(path.join(__dirname, 'build/*'))
    // `./build/*` cleared, but we need a `./build/css` folder user for SASS compilation output.
  fse.mkdir(path.join(__dirname, 'build/css'))
}

/**
 * Compile SASS files into CSS files with MAPS.
 * @param {Object} config of the SASS compilation files.
 */

 function renderSass (config) {
  let idx
  let maxFiles = config.files.length

  for (idx = 0; idx < maxFiles; idx += 1) {
    const filename = path.join(__dirname, config.bld, path.basename(config.files[idx], '.scss'))

    let result = sass.renderSync({
      file: path.join(__dirname, config.src, config.files[idx]),
      outFile: path.join(__dirname, config.bld, path.basename(config.files[idx], '.scss')), // Needed by `sourceMap`
      outputStyle: 'compact', // Values: nested, expanded, compact, compressed
      sourceMap: true,
      sourceMapContents: false,
      sourceMapEmbed: false,
      sourceMapRoot: path.join(__dirname, config.bld)
    })

    fse.writeFile(filename + '.css', result.css, function (error) {
      if (error) {
        console.log(`SASS '${result.stats.entry}' in ERROR`)
        console.log(error)
      }
    })

    fse.writeFile(filename + '.map', result.map, function (error) {
      if (error) {
        console.log(`Error writing MAP file: ${filename + '.map'}`)
        return console.error(error)
      }
    })
  }
}

/**
 * Copy files and maps from location A to B.
 * @param {Object} config of the to be copied files and maps.
 */

function copyAssets(config) {
  let idx
  let max = config.maps.length

  for (idx = 0; idx < max; idx += 1) {
    const from = path.join(__dirname, config.src, config.maps[idx])
    const to = path.join(__dirname, config.bld, config.maps[idx])

    fse.copy(from, to, function (err) {
      if (err){
          console.log('An error occured while copying the folder.')
          return console.error(err)
      }
    })
  }
}

// Step 1: Clear build
cleanBuild()
// Step 2: Compile SASS files
renderSass(sassConfig)
// Step 3: Copy assets (IMG, JS, CSS) to `./build/`
copyAssets(copyAssetsConfig)
// Step 4: Copy web pages to `./build/`
copyAssets(copyHtmlConfig)
// Step 5: Start browser and drag `./build/index.html` into the browser
