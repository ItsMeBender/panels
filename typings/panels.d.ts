import { Panels } from "../src/assets/js/panels";

type Semaphore = 'NONE'|'LOW'|'MEDIUM'|'HIGH'
type SemaphoreType = 'NONE'|'DOWN'|'EQUAL'|'UP'
// `CLOSE` added as enumarated type, because it was a quick way to implement preset CLOSE button.
type State = 'MINIMAL'|'SUMMARY'|'EXPANDED'|'ENLARGED'|'CLOSE'
// Has PANEL a FIXED (sticky) panel system forced order
type Sticky = 'BOTTOM'|'NONE'|'TOP'

interface GridCell {
  column:number;
  panelID:string;
  row:number;
  seq:number;
}

interface GridSize {
  columns:number;
  rows:number;
}

// TODO: Panel state 'default', 'current' , 'previous', share the same properties. 
interface Panel {
  active:boolean;
  availableStates:State[];
  columnSize:number;
  currentState:State;
  defaultActive:boolean;
  defaultSeq:number; // The original sequence, given as HTML attribute
  defaultState:State;
  el:HTMLElement; // Panel HTML DIV
  hasData: boolean; // If MEDIA content is available, to avoid clicking on empty data
  groups:string[];
  id:string;
  previousState:State;
  rowSize:number;
  semaphore:Semaphore;
  semaphoreType:SemaphoreType;
  seq:number;
  startColumn:number;
  startRow:number;
  stickyBottom:boolean;
  stickyTop:boolean;
  title:string;
}

interface PanelFilter {
  isActive?:boolean;
  exclude?:string | string[];
  groups?:string | string[]; // TODO: Not sure this one is used ...
}

interface PanelStateSet {
  MINIMAL:{columnSize:number, rowSize:number, el:HTMLElement};
  SUMMARY:{columnSize:number, rowSize:number, el:HTMLElement};
  EXPANDED:{columnSize:number, rowSize:number, el:HTMLElement};
  ENLARGED:{columnSize:number, rowSize:number, el:HTMLElement};
  CLOSE:{columnSize:number, rowSize:number, el:HTMLElement};
}

interface PanelSeq {
  id:string;
  state:State;
}

interface StateVisibility {
  [key:string]:boolean;
}