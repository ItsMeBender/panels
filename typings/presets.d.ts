import { Presets } from "../src/assets/js/presets";
import { Semaphore, SemaphoreType, State } from "../typings/panels";

// API for filtering Presets
interface PresetFilter {
    panelIDs?:string | string[]
}

// TODO: Name 'PresetSet' is a bit dubious, what is 'set'.
//       It's just the same as Class 'Preset'
interface PresetSet {
    callBack:Function;
    description:string;
    label:string;
    role:string;
    stateSet:StateSet[];
}

// StateSet as in:
// - DEFAULT, As open in screen (preset user defaults).
// - CURRENT, as seen on screen now.
// - PREVIOUS, panel state after change.
interface StateSet {
    id?:string; // Panel ID
    title?:string; // Panel title, for short panel reference info
    hasData:boolean; // Contains the panel DATA? If 'FALSE' show "GEEN DATA BESCHIKBAAR"
    semaphore:Semaphore; // Attention HIGH, LOW ...
    semaphoreType:SemaphoreType; // Data going UP, DOWN ...
    state:State; // New PANEL STATE
    seq:number; // SEQuence in the view.
}
